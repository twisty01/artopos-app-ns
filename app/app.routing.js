"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var home_component_1 = require("./View/home.component");
var painting_component_1 = require("./View/painting.component");
var register_component_1 = require("./View/register.component");
var filter_component_1 = require("./View/filter.component");
var paintingImage_component_1 = require("./View/components/paintingImage.component");
var addPost_component_1 = require("./View/addPost.component");
var routes = [
    { path: "", component: home_component_1.HomeComponent },
    { path: "addPost/:id", component: addPost_component_1.AddPostComponent },
    { path: "painting/:id", component: painting_component_1.PaintingComponent },
    { path: "painting/image/:id", component: paintingImage_component_1.PaintingImageComponent },
    { path: "register", component: register_component_1.RegisterComponent },
    { path: "filter", component: filter_component_1.FilterComponent },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forRoot(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
