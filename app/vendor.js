// Snapshot the ~/app.css and the theme
var application = require("application");
require("ui/styling/style-scope");
var requireAny = require;
var appCssContext = requireAny.context("~/", false, /^\.\/app\.(css|scss|less|sass)$/);
var globalAny = global;
globalAny.registerWebpackModules(appCssContext);
application.loadAppCss();
require("./vendor-platform");
require("reflect-metadata");
require("@angular/platform-browser");
require("@angular/core");
require("@angular/common");
require("@angular/forms");
require("@angular/http");
require("@angular/router");
require("nativescript-angular/platform-static");
require("nativescript-angular/forms");
require("nativescript-angular/router");
