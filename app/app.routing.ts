import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { HomeComponent } from "./View/home.component";
import {PaintingComponent} from "./View/painting.component";
import {RegisterComponent} from "./View/register.component";
import {FilterComponent} from "./View/filter.component";
import { PaintingImageComponent } from "./View/components/paintingImage.component";
import { AddPostComponent } from "./View/addPost.component";

const routes: Routes = [
    { path: "", component: HomeComponent },
    { path: "addPost/:id", component: AddPostComponent },
    { path: "painting/:id", component: PaintingComponent },
    { path: "painting/image/:id", component: PaintingImageComponent },
    { path: "register", component: RegisterComponent},
    { path: "filter", component: FilterComponent},
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }