import {Injectable} from "@angular/core";
import {Painting} from "../Model/painting";
import {forEach} from "@angular/router/src/utils/collection";
import {Column, Entity} from "../Model/entity";

let Sqlite = require("nativescript-sqlite");

const DATABASE_NAME = "artopos.db";


/**
 * DatabaseService manages sqlite database and simplify queries.
 */
@Injectable()
export class DatabaseService {
    private database: any = null;
    private createPromise;

    constructor(){
        this.createPromise = this.createDB();
    }
    public createDB(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.database) {
                reject("database already created");
                return;
            }
            (new Sqlite(DATABASE_NAME)).then(db => {
                db.resultType(Sqlite.RESULTSASOBJECT);
                this.database = db;
                resolve();
            }, error => {
                console.log("OPEN DB ERROR", error);
                reject();
            });
        });
    }

    // basic - wrappers
    public execSQL(sql: string, params = []): Promise<any> {
        if ( !this.database && this.createPromise ){
            return this.createDB()
                .then(()=> this.database.execSQL(sql, params))
                .catch(er=>{console.log("exec sql error: " + er)});
        }
        return this.database.execSQL(sql, params);
    }

    public all(sql: string, params = []): Promise<any> {
        if (!this.database) {
            return new Promise((resolve, reject) => {
                reject("NO DB");
            });
        }
        return this.database.all(sql, params);
    }

    public get(sql: string, params = []): Promise<any> {
        if (!this.database) {
            return new Promise((resolve, reject) => {
                reject("NO DB");
            });
        }
        return this.database.get(sql, params);
    }

    public save(entity: Entity): Promise<any> {
        const cols = entity.columns();
        const marks = Array(cols.length).fill("?").join();
        let params = cols.map(col=>entity[col.name]);
        return this.database.execSQL('INSERT INTO '+entity.tableName()+' ('+cols.map(col=>col.name)+') '
            + 'VALUES ('+marks+')', params).catch((err) => console.log("could not save painting:" + err));
    }

    public createTable(tableName: string, columns: Array<Column>): Promise<any> {
        let sql="CREATE TABLE IF NOT EXISTS "+tableName+" ("+columns.map(col=>col.name+" "+col.type).join()+")";
        return this.execSQL(sql);
    }

    public deleteAllRows(tableName: string): Promise<any> {
        // console.log("Delete all rows: DELETE FROM " + tableName);
        return this.execSQL("DELETE FROM " + tableName);
    }

}