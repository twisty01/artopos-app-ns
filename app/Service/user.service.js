"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_1 = require("../Model/user");
var nativescript_secure_storage_1 = require("nativescript-secure-storage");
var api_service_1 = require("./api.service");
var http_service_1 = require("./http.service");
var KEY_USER = "artopos.currentUser";
var KEY_ACCESS_TOKEN = "artopos.currentUser.accessToken";
var KEY_REFRESH_TOKEN = "artopos.currentUser.refreshToken";
var KEY_EXPIRE_DATE = "artopos.currentUser.expireDate";
var AUTH_URL = "/oauth/v2/token";
var clientId = "6_4h2q66dz1ksgoocwk4ksgk0wksww4sc4w0wgoss4s8ko44cw4o";
var clientSecret = "4rpyq7txeveo88ssw0cgwow4884k0g8oko4gkgogcs4g84kssk";
var EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
/**
 * UserService provides authetication methods, access token, methods for registration and login.
 */
var UserService = (function () {
    function UserService(apiService, http) {
        this.apiService = apiService;
        this.http = http;
        this.loggedIn = false;
        this.secureStorage = new nativescript_secure_storage_1.SecureStorage();
        var token = this.secureStorage.getSync({ key: KEY_ACCESS_TOKEN });
        if (token)
            this.loggedIn = true;
    }
    // constructor(private translate: TranslateService) {}
    UserService.prototype.getCurrentUser = function () {
        var _this = this;
        return this.getTokenParam()
            .then(function (t) { return _this.apiService.get("/users/current", t); }).catch(function (err) { return console.log("error api get :" + JSON.stringify(err)); })
            .then(function (res) {
            console.log(JSON.stringify(res));
            var u = new user_1.User();
            u.email = res['email'];
            u.username = res['username'];
            u.id = res['id'];
            return u;
        });
    };
    UserService.prototype.isLogged = function () {
        return this.loggedIn;
    };
    UserService.prototype.tryLogin = function (email, plainPassword) {
        var _this = this;
        this.logout();
        return new Promise(function (resolve, reject) {
            if (email && !email.match(EMAIL_REGEX)) {
                reject(new Error("ErrorWrongEmail"));
                return;
            }
            else if (!plainPassword) {
                reject(new Error("ErrorMissingPassword"));
                return;
            }
            var url = http_service_1.SERVER_URL + AUTH_URL + _this.getUrlParams(plainPassword, email);
            _this.http.getJSON(url).then(function (r) {
                if (!_this.validateResponse(r)) {
                    reject(new Error("Bad Token Response"));
                    return;
                }
                _this.loggedIn = true;
                _this.getCurrentUser().then(function (r) { return resolve(r); }).catch(function (err) { return console.log("error get current user : + " + err); });
            }).catch(function (err) { return reject(err); });
        });
    };
    UserService.prototype.logout = function () {
        this.secureStorage.remove({ key: KEY_ACCESS_TOKEN });
        this.secureStorage.remove({ key: KEY_REFRESH_TOKEN });
        this.secureStorage.remove({ key: KEY_USER });
        this.secureStorage.remove({ key: KEY_EXPIRE_DATE });
        this.loggedIn = false;
    };
    UserService.prototype.register = function (username, email, password) {
        var _this = this;
        return this.apiService.post("/users", { username: username, email: email, password: password })
            .then(function (res) {
            console.log("REGISTER RES:" + JSON.stringify(res));
            if (res.status >= 400) {
                return new Promise(function (resolve, reject) { return reject(res); });
            }
            return _this.tryLogin(email, password);
        });
    };
    UserService.prototype.getTokenParam = function () {
        return this.getToken().then(function (t) { return "?access_token=" + t; });
    };
    UserService.prototype.getToken = function () {
        if (!this.isLogged()) {
            return new Promise(function (resolve, reject) { return reject(new Error("Error getting token: user isnt logged in")); });
        }
        var expireDate = this.secureStorage.getSync({ key: KEY_EXPIRE_DATE });
        if ((new Date()).getTime() > (new Date(expireDate)).getTime()) {
            return this.refreshToken();
        }
        return this.secureStorage.get({ key: KEY_ACCESS_TOKEN });
    };
    UserService.prototype.getUrlParams = function (password, email) {
        return "?grant_type=password&client_id=" + clientId + "&client_secret=" + clientSecret + "&password=" + password + "&username=" + email;
    };
    UserService.prototype.validateResponse = function (r) {
        var accessToken = r['access_token'];
        var refreshToken = r['refresh_token'];
        var expiresIn = r['expires_in'];
        if (!accessToken || !refreshToken || !expiresIn) {
            console.log("validate response:" + JSON.stringify(r));
            return false;
        }
        var expireDate = new Date();
        expireDate = new Date(expireDate.getTime() + 1000 * (expiresIn - 60)); //1 min reserve
        this.secureStorage.setSync({ key: KEY_ACCESS_TOKEN, value: accessToken });
        this.secureStorage.setSync({ key: KEY_REFRESH_TOKEN, value: refreshToken });
        this.secureStorage.setSync({ key: KEY_EXPIRE_DATE, value: expireDate.toDateString() });
        return true;
    };
    UserService.prototype.refreshToken = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.getJSON(http_service_1.SERVER_URL + AUTH_URL + "?grant_type=refresh_token&client_id=" + clientId + "&client_secret=" +
                clientSecret + "&refresh_token=" + _this.secureStorage.getSync({ key: KEY_REFRESH_TOKEN }))
                .then(function (r) {
                if (_this.validateResponse(r)) {
                    resolve(_this.secureStorage.getSync({ key: KEY_ACCESS_TOKEN }));
                }
                else {
                    reject(new Error("Invalid response"));
                }
            }).catch(function (err) { return reject(err); });
        });
    };
    UserService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService, http_service_1.HttpService])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
