"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Platform = require("tns-core-modules/platform");
var nativescript_google_places_autocomplete_1 = require("nativescript-google-places-autocomplete");
var API_KEY = "AIzaSyAGuUgsK4edxpU1Ngw1EAhkiDYFZf8okh8";
var googlePlacesAutocomplete = new nativescript_google_places_autocomplete_1.GooglePlacesAutocomplete(API_KEY);
/**
 * GeocodingService provides methods for javascript googlePlacesAutocomplete API.
 */
var GeocodingService = (function () {
    function GeocodingService() {
    }
    GeocodingService.prototype.getPlaces = function (params) {
        return googlePlacesAutocomplete.search(params);
    };
    GeocodingService.prototype.getPlace = function (place) {
        return googlePlacesAutocomplete.getPlaceById(place.placeId);
    };
    GeocodingService.boundsToZoom = function (west, east) {
        var angle = east - west;
        if (angle < 0) {
            angle += 360;
        }
        return Math.round(Math.log(Platform.screen.mainScreen.widthPixels * 360 / angle / this.GLOBE_WIDTH) / Math.LN2);
    };
    GeocodingService.GLOBE_WIDTH = 256; // a constant in Google's map projection
    GeocodingService = __decorate([
        core_1.Injectable()
    ], GeocodingService);
    return GeocodingService;
}());
exports.GeocodingService = GeocodingService;
