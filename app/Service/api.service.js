"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_service_1 = require("./http.service");
exports.API_URL = http_service_1.SERVER_URL + "/api";
/**
 * ApiService provides required methods for GET and POST to REST API.
 */
var ApiService = (function () {
    function ApiService(http) {
        this.http = http;
    }
    ApiService.prototype.get = function (resource, params) {
        if (params === void 0) { params = ""; }
        return this.http.getJSON(this.resUrl(resource, params));
    };
    ApiService.prototype.post = function (resource, data, params) {
        if (params === void 0) { params = ""; }
        return this.http.post(this.resUrl(resource, params), JSON.stringify(data));
    };
    ApiService.prototype.deleteResource = function (resource, params) {
        if (params === void 0) { params = ""; }
        var r = this.resUrl(resource, params);
        return this.http._delete(r);
    };
    ApiService.prototype.resUrl = function (r, p) {
        return exports.API_URL + (r[0] == "/" ? "" : "/") + r + (p ? ((p[0] == "?" ? "" : "?") + p) : "");
    };
    ApiService.prototype.postJSON = function (resource, data, params) {
        return this.http.postJSON(this.resUrl(resource, params), JSON.stringify(data));
    };
    ApiService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_service_1.HttpService])
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;
