"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var post_1 = require("../Model/post");
var api_service_1 = require("./api.service");
var user_service_1 = require("./user.service");
/**
 * PostService manages API calls for Post resource.
 */
var PostService = (function () {
    function PostService(api, userService) {
        this.api = api;
        this.userService = userService;
    }
    PostService.prototype.getPostsByPainting = function (paintingId) {
        var _this = this;
        return this.api.get("/paintings/" + paintingId + "/posts").then(function (res) { return res.map(function (po) { return _this.mapPost(po); }); });
    };
    PostService.prototype.setPostsByUser = function (userId) {
        var _this = this;
        return this.api.get("/users/" + userId + "/posts").then(function (res) { return res.map(function (po) { return _this.mapPost(po); }); });
    };
    PostService.prototype.savePost = function (post) {
        var _this = this;
        return this.userService.getTokenParam().then(function (t) { return _this.api.postJSON("/posts", post, t); });
    };
    PostService.prototype.deletePost = function (id) {
        var _this = this;
        return this.userService.getTokenParam().then(function (t) { return _this.api.deleteResource("/posts/" + id, t); });
    };
    PostService.prototype.mapPost = function (o) {
        if (o.created && o.created.length > 10)
            o.created = o.created.substring(0, 10);
        return new post_1.Post().loadFromQueryResult(o);
    };
    PostService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [api_service_1.ApiService, user_service_1.UserService])
    ], PostService);
    return PostService;
}());
exports.PostService = PostService;
