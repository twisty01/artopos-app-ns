import {Injectable} from "@angular/core";
import {HttpService, SERVER_URL} from "./http.service";
import {Response} from "@angular/http";

export const API_URL = SERVER_URL + "/api";

/**
 * ApiService provides required methods for GET and POST to REST API.
 */
@Injectable()
export class ApiService {

    constructor(private http: HttpService){}

    get(resource, params = ""): Promise<any> {
        return this.http.getJSON(this.resUrl(resource, params));
    }

    post(resource, data, params = ""): Promise<any> {
        return this.http.post(this.resUrl(resource, params), JSON.stringify(data));
    }

    deleteResource(resource, params = ""): Promise<any> {
        const r = this.resUrl(resource, params);
        return this.http._delete(r);
    }

    private resUrl(r, p) {
        return API_URL + (r[0] == "/" ? "" : "/") + r + (p ? ((p[0] == "?" ? "" : "?") + p) : "");
    }

    postJSON(resource: string, data: any, params: string) {
        return this.http.postJSON(this.resUrl(resource, params), JSON.stringify(data));
    }
}