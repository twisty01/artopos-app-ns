"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var painting_1 = require("../Model/painting");
var database_service_1 = require("./database.service");
var api_service_1 = require("./api.service");
var Toast = require("nativescript-toast");
var prefs = require("tns-core-modules/application-settings");
var LAST_PAINTINGS_UPDATE = "artopos.last_paintings_update";
var RELOAD_TIME = 1000 * 60 * 60; // 1 h
/**
 * PaintingService manages loading Painting data from API and Saving them to database.
 * PaintingService also provides methods for getting Paintings from database.
 */
var PaintingService = (function () {
    function PaintingService(db, api) {
        this.db = db;
        this.api = api;
        this.paintingsLoaded = false;
        db.createTable(painting_1.Painting.TABLE_NAME, painting_1.Painting.COLUMNS).then(function () {
            // console.log('painting table created');
        }, function (error) {
            console.log('could not create Painting table..' + error);
        });
    }
    PaintingService.prototype.getPaintingsByFilterOptions = function (filterOptions, name) {
        if (name === void 0) { name = null; }
        var selectString = "SELECT * FROM " + painting_1.Painting.TABLE_NAME;
        var queryParams = [];
        var first = true;
        var b = filterOptions.bounds;
        if (b && b.southwest && b.northeast) {
            selectString += " WHERE lat > ? AND lat < ? AND lng > ? AND lng < ? ";
            queryParams.push(b.southwest.latitude, b.northeast.latitude, b.southwest.longitude, b.northeast.longitude);
            first = false;
        }
        if (filterOptions.resolved !== null && typeof filterOptions.resolved != 'undefined') {
            selectString += first ? " WHERE " : " AND ";
            selectString += " resolved = ? ";
            queryParams.push(filterOptions.resolved);
            first = false;
        }
        if (name) {
            selectString += first ? " WHERE " : " AND ";
            selectString += " ( ( LOWER(authorName) LIKE ? ) OR ( LOWER(name) LIKE ? ) ) ";
            var likeName = "%" + name.toLowerCase() + "%";
            queryParams.push(likeName, likeName);
            first = false;
        }
        if (filterOptions.institutionName) {
            selectString += first ? " WHERE " : " AND ";
            selectString += " institutionName = ? ";
            queryParams.push(filterOptions.institutionName);
            first = false;
        }
        console.log('GET PAINTINGS BY FILTER: ' + selectString);
        console.log('GET PAINTINGS BY FILTER params: ' + queryParams.toString());
        return this.db.all(selectString, queryParams).then(function (res) {
            console.log("Filtered results:" + res.length);
            var resPaintings = [];
            for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                var paintingRes = res_1[_i];
                resPaintings.push((new painting_1.Painting()).loadFromQueryResult(paintingRes));
            }
            return resPaintings;
        });
    };
    PaintingService.prototype.getPaintingsByBounds = function (bounds) {
        var selectString = "SELECT * FROM " + painting_1.Painting.TABLE_NAME + " WHERE lat > ? AND lat < ? AND lng > ? AND lng < ?";
        var queryParams = [bounds.southwest.latitude, bounds.northeast.latitude, bounds.southwest.longitude, bounds.northeast.longitude];
        return this.db.all(selectString, queryParams).then(function (res) {
            var resPaintings = [];
            for (var _i = 0, res_2 = res; _i < res_2.length; _i++) {
                var paintingRes = res_2[_i];
                resPaintings.push((new painting_1.Painting()).loadFromQueryResult(paintingRes));
            }
            return resPaintings;
        });
    };
    PaintingService.prototype.getPainting = function (id) {
        return this.db.get("SELECT * FROM " + painting_1.Painting.TABLE_NAME + " WHERE id = ?", [id])
            .then(function (res) { return (new painting_1.Painting()).loadFromQueryResult(res); });
    };
    PaintingService.prototype.loadPaintings = function () {
        var _this = this;
        if (this.loadingPromise)
            return this.loadingPromise;
        this.paintingsLoaded = false;
        return this.loadingPromise = this.db.deleteAllRows(painting_1.Painting.TABLE_NAME)
            .then(function () {
            return _this.api.get("/paintings").then(function (res) {
                var _loop_1 = function (paintObj) {
                    var paint = new painting_1.Painting().loadFromQueryResult(paintObj);
                    _this.db.save(paint).catch(function (err) { console.log("error saving painting:" + paintObj.id + " => " + err); });
                };
                for (var _i = 0, res_3 = res; _i < res_3.length; _i++) {
                    var paintObj = res_3[_i];
                    _loop_1(paintObj);
                }
                prefs.setNumber(LAST_PAINTINGS_UPDATE, (new Date).getTime());
                _this.paintingsLoaded = true;
                _this.loadingPromise = null;
            });
        }).catch(function (err) {
            console.log("error loading paintings: " + JSON.stringify(err));
            Toast.makeText("Unable to load data:" + JSON.stringify(err), "long").show();
        });
    };
    PaintingService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [database_service_1.DatabaseService, api_service_1.ApiService])
    ], PaintingService);
    return PaintingService;
}());
exports.PaintingService = PaintingService;
