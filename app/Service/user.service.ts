import {Injectable} from "@angular/core";
import {User} from "../Model/user";
import {SecureStorage} from "nativescript-secure-storage";
import {ApiService} from "./api.service";
import {HttpService, SERVER_URL} from "./http.service";

const KEY_USER = "artopos.currentUser";
const KEY_ACCESS_TOKEN = "artopos.currentUser.accessToken";
const KEY_REFRESH_TOKEN = "artopos.currentUser.refreshToken";
const KEY_EXPIRE_DATE = "artopos.currentUser.expireDate";

const AUTH_URL = "/oauth/v2/token";
const clientId = "6_4h2q66dz1ksgoocwk4ksgk0wksww4sc4w0wgoss4s8ko44cw4o";
const clientSecret = "4rpyq7txeveo88ssw0cgwow4884k0g8oko4gkgogcs4g84kssk";

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


/**
 * UserService provides authetication methods, access token, methods for registration and login.
 */
@Injectable()
export class UserService {
    loggedIn = false;

    private secureStorage: SecureStorage = new SecureStorage();

    constructor(private apiService: ApiService, private http: HttpService) {
        let token = this.secureStorage.getSync({key: KEY_ACCESS_TOKEN});
        if (token) this.loggedIn = true;
    }

    // constructor(private translate: TranslateService) {}

    getCurrentUser(): Promise<User> {
        return this.getTokenParam()
            .then(t => this.apiService.get("/users/current", t)).catch(err => console.log("error api get :" + JSON.stringify(err)))
            .then(res => {
                console.log(JSON.stringify(res));
                let u = new User();
                u.email = res['email'];
                u.username = res['username'];
                u.id = res['id'];
                return u;
            })
    }

    isLogged() {
        return this.loggedIn;
    }

    tryLogin(email: string, plainPassword: string): Promise<User> {
        this.logout();
        return new Promise<User>((resolve, reject) => {
                if (email && !email.match(EMAIL_REGEX)) {
                    reject(new Error("ErrorWrongEmail"));
                    return
                } else if (!plainPassword) {
                    reject(new Error("ErrorMissingPassword"));
                    return
                }
                let url = SERVER_URL + AUTH_URL + this.getUrlParams(plainPassword, email);
                this.http.getJSON(url).then((r) => {
                    if (!this.validateResponse(r)) {
                        reject(new Error("Bad Token Response"));
                        return;
                    }
                    this.loggedIn = true;
                    this.getCurrentUser().then(r => resolve(r)).catch(err => console.log("error get current user : + " + err));
                }).catch(err => reject(err));
            }
        );
    }

    logout() {
        this.secureStorage.remove({key: KEY_ACCESS_TOKEN});
        this.secureStorage.remove({key: KEY_REFRESH_TOKEN});
        this.secureStorage.remove({key: KEY_USER});
        this.secureStorage.remove({key: KEY_EXPIRE_DATE});
        this.loggedIn = false;
    }

    register(username: string, email: string, password: string): Promise<User> {
        return this.apiService.post("/users", {username, email, password})
            .then(res => {
                console.log("REGISTER RES:" + JSON.stringify(res));
                if (res.status >= 400) {
                    return new Promise<User>((resolve, reject) => reject(res));
                }
                return this.tryLogin(email, password);
            });
    }

    getTokenParam(): Promise<string> {
        return this.getToken().then(t => "?access_token=" + t);
    }

    getToken(): Promise<string> {
        if (!this.isLogged()) {
            return new Promise<string>((resolve, reject) => reject(new Error("Error getting token: user isnt logged in")));
        }
        const expireDate = this.secureStorage.getSync({key: KEY_EXPIRE_DATE});
        if ((new Date()).getTime() > (new Date(expireDate)).getTime()) {
            return this.refreshToken();
        }
        return this.secureStorage.get({key: KEY_ACCESS_TOKEN});
    }

    private getUrlParams(password, email) {
        return "?grant_type=password&client_id=" + clientId + "&client_secret=" + clientSecret + "&password=" + password + "&username=" + email
    }

    private validateResponse(r) {
        let accessToken = r['access_token'];
        let refreshToken = r['refresh_token'];
        let expiresIn = r['expires_in'];
        if (!accessToken || !refreshToken || !expiresIn) {
            console.log("validate response:" + JSON.stringify(r));
            return false;
        }
        let expireDate = new Date();
        expireDate = new Date(expireDate.getTime() + 1000 * (expiresIn - 60)); //1 min reserve
        this.secureStorage.setSync({key: KEY_ACCESS_TOKEN, value: accessToken});
        this.secureStorage.setSync({key: KEY_REFRESH_TOKEN, value: refreshToken});
        this.secureStorage.setSync({key: KEY_EXPIRE_DATE, value: expireDate.toDateString()});
        return true;
    }

    private refreshToken(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.http.getJSON(SERVER_URL + AUTH_URL + "?grant_type=refresh_token&client_id=" + clientId + "&client_secret=" +
                clientSecret + "&refresh_token=" + this.secureStorage.getSync({key: KEY_REFRESH_TOKEN}))
                .then(r => {
                    if (this.validateResponse(r)) {
                        resolve(this.secureStorage.getSync({key: KEY_ACCESS_TOKEN}));
                    } else {
                        reject(new Error("Invalid response"));
                    }
                }).catch(err => reject(err));
        })
    }

}