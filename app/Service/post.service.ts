import {Injectable} from "@angular/core";
import {Post} from "../Model/post";
import {ApiService} from "./api.service";
import {Painting} from "../Model/painting";
import {UserService} from "./user.service";
import {Entity} from "../Model/entity";

/**
 * PostService manages API calls for Post resource.
 */
@Injectable()
export class PostService {

    constructor(private api: ApiService, private userService: UserService) {}

    getPostsByPainting(paintingId: number): Promise<Array<Post>> {
        return this.api.get(`/paintings/${paintingId}/posts`).then(res => res.map(po => this.mapPost(po)))
    }

    setPostsByUser(userId: number): Promise<Array<Post>> {
        return this.api.get(`/users/${userId}/posts`).then(res => res.map(po => this.mapPost(po)))
    }

    savePost(post: Post): Promise<any> {
        return this.userService.getTokenParam().then(t => this.api.postJSON(`/posts`, post, t))
    }

    deletePost(id) {
        return this.userService.getTokenParam().then(t => this.api.deleteResource(`/posts/` + id, t));
    }

    private mapPost(o): Entity {
        if (o.created && o.created.length > 10) o.created = o.created.substring(0, 10);
        return new Post().loadFromQueryResult(o);
    }

}


