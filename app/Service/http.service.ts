import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable as RxObservable} from "rxjs/Observable";
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

export const SERVER_URL = "https://artopos.000webhostapp.com";

/**
 * HttpService manages HTTP requests, should by mostly used by ApiService.
 */
@Injectable()
export class HttpService {
    constructor(private http: HttpClient) { }

    getJSON(url): Promise<any> {
        return this.http.get(url).toPromise();
    }

    post(url, content): Promise<any> {
        return this.http.post(url, content,{responseType:"text"}).toPromise()
    }

    postJSON(url, content): Promise<any> {
        return this.http.post(url, content).toPromise()
    }

    _delete(url): Promise<any> {
        return this.http.delete(url,{responseType:"text"}).toPromise();
    }

}