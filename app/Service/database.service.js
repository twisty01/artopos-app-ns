"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Sqlite = require("nativescript-sqlite");
var DATABASE_NAME = "artopos.db";
/**
 * DatabaseService manages sqlite database and simplify queries.
 */
var DatabaseService = (function () {
    function DatabaseService() {
        this.database = null;
        this.createPromise = this.createDB();
    }
    DatabaseService.prototype.createDB = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.database) {
                reject("database already created");
                return;
            }
            (new Sqlite(DATABASE_NAME)).then(function (db) {
                db.resultType(Sqlite.RESULTSASOBJECT);
                _this.database = db;
                resolve();
            }, function (error) {
                console.log("OPEN DB ERROR", error);
                reject();
            });
        });
    };
    // basic - wrappers
    DatabaseService.prototype.execSQL = function (sql, params) {
        var _this = this;
        if (params === void 0) { params = []; }
        if (!this.database && this.createPromise) {
            return this.createDB()
                .then(function () { return _this.database.execSQL(sql, params); })
                .catch(function (er) { console.log("exec sql error: " + er); });
        }
        return this.database.execSQL(sql, params);
    };
    DatabaseService.prototype.all = function (sql, params) {
        if (params === void 0) { params = []; }
        if (!this.database) {
            return new Promise(function (resolve, reject) {
                reject("NO DB");
            });
        }
        return this.database.all(sql, params);
    };
    DatabaseService.prototype.get = function (sql, params) {
        if (params === void 0) { params = []; }
        if (!this.database) {
            return new Promise(function (resolve, reject) {
                reject("NO DB");
            });
        }
        return this.database.get(sql, params);
    };
    DatabaseService.prototype.save = function (entity) {
        var cols = entity.columns();
        var marks = Array(cols.length).fill("?").join();
        var params = cols.map(function (col) { return entity[col.name]; });
        return this.database.execSQL('INSERT INTO ' + entity.tableName() + ' (' + cols.map(function (col) { return col.name; }) + ') '
            + 'VALUES (' + marks + ')', params).catch(function (err) { return console.log("could not save painting:" + err); });
    };
    DatabaseService.prototype.createTable = function (tableName, columns) {
        var sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + columns.map(function (col) { return col.name + " " + col.type; }).join() + ")";
        return this.execSQL(sql);
    };
    DatabaseService.prototype.deleteAllRows = function (tableName) {
        // console.log("Delete all rows: DELETE FROM " + tableName);
        return this.execSQL("DELETE FROM " + tableName);
    };
    DatabaseService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], DatabaseService);
    return DatabaseService;
}());
exports.DatabaseService = DatabaseService;
