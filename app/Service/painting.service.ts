import {Injectable} from "@angular/core";
import {Painting} from "../Model/painting";
import {DatabaseService} from "./database.service";
import {FilterOptions} from "../View/components/homeList.component";
import {ApiService} from "./api.service";
import * as Toast from "nativescript-toast";
import * as prefs from "tns-core-modules/application-settings";
import {Entity} from "../Model/entity";


const LAST_PAINTINGS_UPDATE = "artopos.last_paintings_update";
const RELOAD_TIME = 1000 * 60 * 60; // 1 h

/**
 * PaintingService manages loading Painting data from API and Saving them to database.
 * PaintingService also provides methods for getting Paintings from database.
 */
@Injectable()
export class PaintingService {
    paintingsLoaded = false;
    loadingPromise;

    constructor(private db: DatabaseService, private api: ApiService) {
        db.createTable(Painting.TABLE_NAME, Painting.COLUMNS).then(() => {
            // console.log('painting table created');
        }, error => {
            console.log('could not create Painting table..' + error)
        });
    }

    getPaintingsByFilterOptions(filterOptions: FilterOptions, name = null) {
        let selectString = "SELECT * FROM " + Painting.TABLE_NAME;
        let queryParams = [];
        let first = true;
        let b = filterOptions.bounds;
        if (b && b.southwest && b.northeast) {
            selectString += " WHERE lat > ? AND lat < ? AND lng > ? AND lng < ? ";
            queryParams.push(b.southwest.latitude, b.northeast.latitude, b.southwest.longitude, b.northeast.longitude);
            first = false;
        }
        if (filterOptions.resolved !== null && typeof filterOptions.resolved != 'undefined' ) {
            selectString += first ? " WHERE " : " AND ";
            selectString += " resolved = ? ";
            queryParams.push(filterOptions.resolved);
            first = false;
        }
        if (name) {
            selectString += first ? " WHERE " : " AND ";
            selectString += " ( ( LOWER(authorName) LIKE ? ) OR ( LOWER(name) LIKE ? ) ) ";
            const likeName = "%"+name.toLowerCase()+"%";
            queryParams.push(likeName, likeName);
            first = false;
        }
        if (filterOptions.institutionName) {
            selectString += first ? " WHERE " : " AND ";
            selectString += " institutionName = ? ";
            queryParams.push(filterOptions.institutionName);
            first = false;
        }

        console.log('GET PAINTINGS BY FILTER: ' + selectString);
        console.log('GET PAINTINGS BY FILTER params: ' + queryParams.toString());
        return this.db.all(
            selectString,
            queryParams
        ).then(res => {
            console.log("Filtered results:" + res.length);
            let resPaintings = [];
            for (let paintingRes of res) {
                resPaintings.push((new Painting()).loadFromQueryResult(paintingRes));
            }
            return resPaintings;
        });
    }

    getPaintingsByBounds(bounds) {
        let selectString = "SELECT * FROM " + Painting.TABLE_NAME + " WHERE lat > ? AND lat < ? AND lng > ? AND lng < ?";
        let queryParams = [bounds.southwest.latitude, bounds.northeast.latitude, bounds.southwest.longitude, bounds.northeast.longitude];
        return this.db.all(
            selectString,
            queryParams
        ).then(res => {
            let resPaintings = [];
            for (let paintingRes of res) {
                resPaintings.push((new Painting()).loadFromQueryResult(paintingRes));
            }
            return resPaintings;
        });
    }

    getPainting(id: number): Promise<Painting> {
        return this.db.get("SELECT * FROM " + Painting.TABLE_NAME + " WHERE id = ?", [id])
            .then(res => <Painting>(new Painting()).loadFromQueryResult(res));
    }

    loadPaintings(): Promise<any> {
        if (this.loadingPromise) return this.loadingPromise;

        this.paintingsLoaded = false;

        return this.loadingPromise = this.db.deleteAllRows(Painting.TABLE_NAME)
            .then(() => {
                return this.api.get("/paintings").then(res => {
                    for (let paintObj of res) {
                        let paint = new Painting().loadFromQueryResult(paintObj);
                        this.db.save(paint).catch(err => {console.log("error saving painting:" + paintObj.id + " => " + err)});
                    }
                    prefs.setNumber(LAST_PAINTINGS_UPDATE, (new Date).getTime());
                    this.paintingsLoaded = true;
                    this.loadingPromise = null;
                })
            }).catch((err) => {
                console.log("error loading paintings: " + JSON.stringify(err));
                Toast.makeText("Unable to load data:" + JSON.stringify(err), "long").show();
            })
    }
}

