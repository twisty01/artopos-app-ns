import {Injectable} from "@angular/core";
import * as Platform from "tns-core-modules/platform";
import { GooglePlacesAutocomplete } from 'nativescript-google-places-autocomplete';

let API_KEY = "AIzaSyAGuUgsK4edxpU1Ngw1EAhkiDYFZf8okh8";
let googlePlacesAutocomplete = new GooglePlacesAutocomplete(API_KEY);

/**
 * GeocodingService provides methods for javascript googlePlacesAutocomplete API.
 */
@Injectable()
export class GeocodingService {

    private static readonly GLOBE_WIDTH = 256; // a constant in Google's map projection

    getPlaces(params){
        return googlePlacesAutocomplete.search(params);
    }

    getPlace(place) {
        return googlePlacesAutocomplete.getPlaceById(place.placeId)
    }

    static boundsToZoom(west, east) {
        let angle = east - west;
        if (angle < 0) {
            angle += 360;
        }
        return Math.round(Math.log(Platform.screen.mainScreen.widthPixels * 360 / angle / this.GLOBE_WIDTH) / Math.LN2);
    }

}