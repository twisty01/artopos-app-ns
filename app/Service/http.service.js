"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
require("rxjs/add/operator/toPromise");
require("rxjs/add/operator/map");
exports.SERVER_URL = "https://artopos.000webhostapp.com";
/**
 * HttpService manages HTTP requests, should by mostly used by ApiService.
 */
var HttpService = (function () {
    function HttpService(http) {
        this.http = http;
    }
    HttpService.prototype.getJSON = function (url) {
        return this.http.get(url).toPromise();
    };
    HttpService.prototype.post = function (url, content) {
        return this.http.post(url, content, { responseType: "text" }).toPromise();
    };
    HttpService.prototype.postJSON = function (url, content) {
        return this.http.post(url, content).toPromise();
    };
    HttpService.prototype._delete = function (url) {
        return this.http.delete(url, { responseType: "text" }).toPromise();
    };
    HttpService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], HttpService);
    return HttpService;
}());
exports.HttpService = HttpService;
