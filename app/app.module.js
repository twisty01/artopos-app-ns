"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var platform = require("platform");
var nativescript_angular_1 = require("nativescript-angular");
var angular_1 = require("nativescript-localize/angular");
var http_client_1 = require("nativescript-angular/http-client");
var http_1 = require("@angular/http");
// COMPONENTS
var home_component_1 = require("./View/home.component");
var statusbarstyles_component_1 = require("./View/components/statusbarstyles.component");
var painting_component_1 = require("./View/painting.component");
var painting_service_1 = require("./Service/painting.service");
var post_service_1 = require("./Service/post.service");
var user_service_1 = require("./Service/user.service");
var register_component_1 = require("./View/register.component");
var homeProfile_component_1 = require("./View/components/homeProfile.component");
var database_service_1 = require("./Service/database.service");
var geocoding_service_1 = require("./Service/geocoding.service");
var nativescript_bottombar_1 = require("nativescript-bottombar");
var homeMap_component_1 = require("./View/components/homeMap.component");
var homeList_component_1 = require("./View/components/homeList.component");
var postItem_component_1 = require("./View/components/postItem.component");
var filter_component_1 = require("./View/filter.component");
var mapSearch_component_1 = require("./View/components/mapSearch.component");
var paintingImage_component_1 = require("./View/components/paintingImage.component");
var addPost_component_1 = require("./View/addPost.component");
var api_service_1 = require("./Service/api.service");
var paintingItem_component_1 = require("./View/components/paintingItem.component");
var http_service_1 = require("./Service/http.service");
// Uncomment and add to NgModule imports if you need to use two-way binding
var forms_1 = require("nativescript-angular/forms");
if (platform.isIOS) {
    GMSServices.provideAPIKey("AIzaSyBJi1VXy_DdF-o8O4Meyxy57HDz8fjTkrY");
}
// TO USE IN HTML
nativescript_angular_1.registerElement("MapView", function () { return require("nativescript-google-maps-sdk").MapView; });
nativescript_angular_1.registerElement("StatusBar", function () { return require("nativescript-statusbar").StatusBar; });
nativescript_angular_1.registerElement('BottomBar', function () { return nativescript_bottombar_1.BottomBar; });
nativescript_angular_1.registerElement("Fab", function () { return require("nativescript-floatingactionbutton").Fab; });
nativescript_angular_1.registerElement("ImageSwipe", function () { return require("nativescript-image-swipe/image-swipe").ImageSwipe; });
nativescript_angular_1.registerElement("StatusBarStyle", function () { return require("./View/components/statusbarstyles.component").StatusbarstyleComponent; });
// registerElement('ImageCacheIt', () => require('nativescript-image-cache-it').ImageCacheIt);
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                app_component_1.AppComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                app_routing_1.AppRoutingModule,
                forms_1.NativeScriptFormsModule,
                http_client_1.NativeScriptHttpClientModule,
                http_1.HttpModule,
                angular_1.NativeScriptLocalizeModule
            ],
            declarations: [
                statusbarstyles_component_1.StatusbarstyleComponent,
                mapSearch_component_1.MapSearchComponent,
                paintingImage_component_1.PaintingImageComponent,
                paintingItem_component_1.PaintingItemComponent,
                home_component_1.HomeComponent,
                homeMap_component_1.HomeMapComponent,
                homeProfile_component_1.HomeProfileComponent,
                homeList_component_1.HomeListComponent,
                filter_component_1.FilterComponent,
                painting_component_1.PaintingComponent,
                postItem_component_1.PostItemComponent,
                addPost_component_1.AddPostComponent,
                register_component_1.RegisterComponent,
                app_component_1.AppComponent,
            ],
            providers: [
                painting_service_1.PaintingService,
                post_service_1.PostService,
                user_service_1.UserService,
                database_service_1.DatabaseService,
                geocoding_service_1.GeocodingService,
                api_service_1.ApiService,
                http_service_1.HttpService
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
