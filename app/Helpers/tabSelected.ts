import {HomeComponent} from "../View/home.component";

/**
 * This interface serves home component tab items change callbacks
 */
export interface TabSelected{
    tabSelected(home: HomeComponent);
    tabDeselected(home: HomeComponent);
}