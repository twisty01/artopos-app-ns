// Snapshot the ~/app.css and the theme
const application = require("application");
require("ui/styling/style-scope");

const requireAny:any = require;
const appCssContext = requireAny.context("~/", false, /^\.\/app\.(css|scss|less|sass)$/);

const globalAny:any = global;
globalAny.registerWebpackModules(appCssContext);
application.loadAppCss();

require("./vendor-platform");

require("reflect-metadata");
require("@angular/platform-browser");
require("@angular/core");
require("@angular/common");
require("@angular/forms");
require("@angular/http");
require("@angular/router");

require("nativescript-angular/platform-static");
require("nativescript-angular/forms");
require("nativescript-angular/router");
