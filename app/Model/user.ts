/**
 * User represents User entity from API.
 */
export class User{
    id: number;
    username: string;
    email: string;
}