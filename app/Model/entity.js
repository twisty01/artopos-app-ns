"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Column servers as database column for serializing entity to sqlite database.
 */
var Column = (function () {
    function Column(name, type) {
        this.name = name;
        this.type = type;
    }
    return Column;
}());
exports.Column = Column;
/**
 * Entity is an abstract class for serializing and deserializing data from API and from database.
 */
var Entity = (function () {
    function Entity() {
    }
    return Entity;
}());
exports.Entity = Entity;
