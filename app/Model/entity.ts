/**
 * Column servers as database column for serializing entity to sqlite database.
 */
export class Column {
    name: string;
    type: string;

    constructor(name: string, type: string) {
        this.name = name;
        this.type = type;
    }
}

/**
 * Entity is an abstract class for serializing and deserializing data from API and from database.
 */
export abstract class Entity {

    constructor() {
    }

    abstract columns(): Array<Column>;
    abstract tableName(): string;
    abstract loadFromQueryResult(res) : Entity;
}