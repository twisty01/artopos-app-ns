"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var entity_1 = require("./entity");
/**
 * Post represents post entity loaded from API.
 */
var Post = (function (_super) {
    __extends(Post, _super);
    function Post(id) {
        if (id === void 0) { id = null; }
        var _this = _super.call(this) || this;
        _this.id = id;
        return _this;
    }
    Post.prototype.columns = function () {
        return Post.COLUMNS;
    };
    Post.prototype.tableName = function () {
        return Post.TABLE_NAME;
    };
    Post.prototype.loadFromQueryResult = function (res) {
        this.id = res.id;
        this.lat = res.lat;
        this.lng = res.lng;
        this.resolved = res.resolved;
        this.approved = res.approved;
        this.text = res.text;
        this.tn = res.tn;
        this.img = res.img;
        this.created = res.created;
        this.userId = res.userId;
        this.paintingId = res.paintingId;
        this.userName = res.userName;
        return this;
    };
    Post.TABLE_NAME = "post";
    Post.COLUMNS = [
        new entity_1.Column('id', 'INTEGER PRIMARY KEY'),
        new entity_1.Column('lat', 'REAL'),
        new entity_1.Column('lng', 'REAL'),
        new entity_1.Column('resolved', 'INTEGER'),
        new entity_1.Column('approved', 'INTEGER'),
        new entity_1.Column('text', 'TEXT'),
        new entity_1.Column('tn', 'TEXT'),
        new entity_1.Column('img', 'TEXT'),
        new entity_1.Column('created', 'TEXT'),
        new entity_1.Column('userId', 'INTEGER'),
        new entity_1.Column('paintingId', 'INTEGER'),
        new entity_1.Column('userName', 'TEXT'),
    ];
    return Post;
}(entity_1.Entity));
exports.Post = Post;
