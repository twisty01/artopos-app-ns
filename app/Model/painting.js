"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var entity_1 = require("./entity");
/**
 * Painting represents Painting entity from API.
 */
var Painting = (function (_super) {
    __extends(Painting, _super);
    function Painting(id, name) {
        if (id === void 0) { id = null; }
        if (name === void 0) { name = null; }
        var _this = _super.call(this) || this;
        _this.id = id;
        _this.name = name;
        return _this;
    }
    Painting.prototype.tableName = function () { return Painting.TABLE_NAME; };
    Painting.prototype.columns = function () { return Painting.COLUMNS; };
    Painting.prototype.loadFromQueryResult = function (res) {
        this.id = res.id;
        this.name = res.name;
        this.lat = res.lat;
        this.lng = res.lng;
        this.resolved = res.resolved;
        this.approved = res.approved;
        this.description = res.description;
        this.perex = res.perex;
        this.img = res.img;
        this.tn = res.tn;
        this.paintingCreated = res.paintingCreated ? res.paintingCreated.substr(0, 4) : null;
        this.authorName = res.authorName;
        this.authorId = res.authorId;
        this.userId = res.userId;
        this.institutionLink = res.institutionLink;
        this.institutionName = res.institutionName;
        return this;
    };
    Painting.TABLE_NAME = "painting";
    Painting.COLUMNS = [
        new entity_1.Column('id', 'INTEGER PRIMARY KEY'),
        new entity_1.Column('name', 'TEXT'),
        new entity_1.Column('lat', 'REAL'),
        new entity_1.Column('lng', 'REAL'),
        new entity_1.Column('resolved', 'INTEGER'),
        new entity_1.Column('approved', 'INTEGER'),
        new entity_1.Column('perex', 'TEXT'),
        new entity_1.Column('description', 'TEXT'),
        new entity_1.Column('img', 'TEXT'),
        new entity_1.Column('tn', 'TEXT'),
        new entity_1.Column('paintingCreated', 'TEXT'),
        new entity_1.Column('authorName', 'TEXT'),
        new entity_1.Column('authorId', 'INTEGER'),
        new entity_1.Column('userId', 'INTEGER'),
        new entity_1.Column('institutionName', 'TEXT'),
        new entity_1.Column('institutionLink', 'TEXT'),
    ];
    return Painting;
}(entity_1.Entity));
exports.Painting = Painting;
