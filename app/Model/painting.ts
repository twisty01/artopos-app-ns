import {Column, Entity} from "./entity";
import * as fecha from "fecha";

/**
 * Painting represents Painting entity from API.
 */
export class Painting extends Entity {
    id: number;

    name: string;
    perex: string;
    description: string;

    img: string;
    tn: string;
    lat: number;
    lng: number;
    paintingCreated: string;

    resolved: boolean;
    approved: boolean;

    authorName: string;
    authorId: number;

    userId: number;

    institutionName: string;
    institutionLink: string;

    public static readonly TABLE_NAME = "painting";

    public static readonly COLUMNS = [
        new Column('id', 'INTEGER PRIMARY KEY'),
        new Column('name', 'TEXT'),
        new Column('lat', 'REAL'),
        new Column('lng', 'REAL'),
        new Column('resolved', 'INTEGER'),
        new Column('approved', 'INTEGER'),
        new Column('perex', 'TEXT'),
        new Column('description', 'TEXT'),
        new Column('img', 'TEXT'),
        new Column('tn', 'TEXT'),
        new Column('paintingCreated', 'TEXT'),
        new Column('authorName', 'TEXT'),
        new Column('authorId', 'INTEGER'),
        new Column('userId', 'INTEGER'),
        new Column('institutionName', 'TEXT'),
        new Column('institutionLink', 'TEXT'),
    ];

    constructor(id: number = null, name: string = null) {
        super();
        this.id = id;
        this.name = name;
    }

    tableName() { return Painting.TABLE_NAME }
    columns() { return Painting.COLUMNS }

    public loadFromQueryResult(res) : Entity{
        this.id = res.id;
        this.name = res.name;
        this.lat = res.lat;
        this.lng = res.lng;
        this.resolved = res.resolved;
        this.approved = res.approved;
        this.description = res.description;
        this.perex = res.perex;
        this.img = res.img;
        this.tn = res.tn;
        this.paintingCreated = res.paintingCreated ? res.paintingCreated.substr(0,4) : null;
        this.authorName = res.authorName;
        this.authorId = res.authorId;
        this.userId = res.userId;
        this.institutionLink = res.institutionLink;
        this.institutionName = res.institutionName;
        return this;
    }
}