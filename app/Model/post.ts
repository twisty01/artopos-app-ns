import { Entity, Column } from "./entity";

/**
 * Post represents post entity loaded from API.
 */
export class Post extends Entity{
    
    id: number;
   
    lat: number;
    lng: number;
    
    resolved: boolean;
    approved: boolean;

    text: string;
    img: string;
    tn: string;

    created: string;

    paintingId;
    
    userId: number;
    userName: string;

    constructor(id: number = null) {
        super();
        this.id = id;
    }

    public static readonly TABLE_NAME = "post";

    public static readonly COLUMNS = [
        new Column('id', 'INTEGER PRIMARY KEY'),
        new Column('lat', 'REAL'),
        new Column('lng', 'REAL'),
        new Column('resolved', 'INTEGER'),
        new Column('approved', 'INTEGER'),
        new Column('text', 'TEXT'),
        new Column('tn', 'TEXT'),
        new Column('img', 'TEXT'),
        new Column('created', 'TEXT'),
        new Column('userId', 'INTEGER'),
        new Column('paintingId', 'INTEGER'),
        new Column('userName', 'TEXT'),
    ];
    
    columns(): Column[] {
        return Post.COLUMNS;
    }
    tableName(): string {
        return Post.TABLE_NAME;
    }

    loadFromQueryResult(res) : Entity{
        this.id = res.id;
        this.lat = res.lat;
        this.lng = res.lng;
        this.resolved = res.resolved;
        this.approved = res.approved;
        this.text = res.text;
        this.tn = res.tn;
        this.img = res.img;
        this.created = res.created;
        this.userId = res.userId;
        this.paintingId = res.paintingId;
        this.userName = res.userName;
        return this;
    }
}