import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core";
import {NativeScriptModule} from "nativescript-angular/nativescript.module";
import {AppRoutingModule} from "./app.routing";
import {AppComponent} from "./app.component";
import * as platform from "platform";
import {registerElement} from "nativescript-angular";
import {NativeScriptLocalizeModule} from "nativescript-localize/angular";
import {NativeScriptHttpClientModule} from "nativescript-angular/http-client";
import { HttpModule } from '@angular/http';
// COMPONENTS
import {HomeComponent} from "./View/home.component";
import {StatusbarstyleComponent} from "./View/components/statusbarstyles.component";
import {PaintingComponent} from "./View/painting.component";
import {PaintingService} from "./Service/painting.service";
import {PostService} from "./Service/post.service";
import {UserService} from "./Service/user.service";
import {RegisterComponent} from "./View/register.component";
import {HomeProfileComponent} from "./View/components/homeProfile.component";
import {DatabaseService} from "./Service/database.service";
import {GeocodingService} from "./Service/geocoding.service";
import {BottomBar} from "nativescript-bottombar";
import {HomeMapComponent} from "./View/components/homeMap.component";
import {HomeListComponent} from "./View/components/homeList.component";
import {PostItemComponent} from "./View/components/postItem.component";
import {FilterComponent} from "./View/filter.component";
import { MapSearchComponent } from "./View/components/mapSearch.component";
import { PaintingImageComponent } from "./View/components/paintingImage.component";
import { AddPostComponent } from "./View/addPost.component";
import {ApiService} from "./Service/api.service";
import {PaintingItemComponent} from "./View/components/paintingItem.component";
import {HttpService} from "./Service/http.service";
// Uncomment and add to NgModule imports if you need to use two-way binding
import {NativeScriptFormsModule} from "nativescript-angular/forms";
// http
declare let GMSServices: any;// GOOGLE MAPS FOR iOS
if (platform.isIOS) {GMSServices.provideAPIKey("AIzaSyBJi1VXy_DdF-o8O4Meyxy57HDz8fjTkrY");}
// TO USE IN HTML
registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
registerElement("StatusBar", () => require("nativescript-statusbar").StatusBar);
registerElement('BottomBar', () => BottomBar);
registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);
registerElement("ImageSwipe", () => require("nativescript-image-swipe/image-swipe").ImageSwipe);
registerElement("StatusBarStyle", () => require("./View/components/statusbarstyles.component").StatusbarstyleComponent);
// registerElement('ImageCacheIt', () => require('nativescript-image-cache-it').ImageCacheIt);

/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        HttpModule,
        NativeScriptLocalizeModule
    ],
    declarations: [
        StatusbarstyleComponent,
        MapSearchComponent,
        PaintingImageComponent,
        PaintingItemComponent,
        HomeComponent,
        HomeMapComponent,
        HomeProfileComponent,
        HomeListComponent,
        FilterComponent,
        PaintingComponent,
        PostItemComponent,
        AddPostComponent,
        RegisterComponent,
        AppComponent,
    ],
    providers: [
        PaintingService,
        PostService,
        UserService,
        DatabaseService,
        GeocodingService,
        ApiService,
        HttpService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule {}
