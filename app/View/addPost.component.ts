import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {RouterExtensions, PageRoute} from "nativescript-angular";
import {UserService} from "../Service/user.service";
import {PostService} from "../Service/post.service";
import {PaintingService} from "../Service/painting.service";
import {User} from "../Model/user";
import {Post} from "../Model/post";
import * as geolocation from "nativescript-geolocation";
import {Page, NavigatedData} from "tns-core-modules/ui/page/page";
import {Painting} from "../Model/painting";
import {MapView, Marker, Position, Bounds} from "nativescript-google-maps-sdk";
import * as dialogs from "tns-core-modules/ui/dialogs";
import {localize} from "nativescript-localize";
import * as imagepicker from "nativescript-imagepicker";
import * as camera from "nativescript-camera";
import * as imageSourceModule from "image-source";
import * as prefs from "tns-core-modules/application-settings";
import {LAST_PAINTING_BOUNDS_KEY, PAINTING_SHOW_POSTS_TAB} from "./painting.component";
import * as Toast from "nativescript-toast";
import * as app from "tns-core-modules/application";
import * as SocialShare from "nativescript-social-share";
import * as clipboard from "nativescript-clipboard";
import * as utils from "tns-core-modules/utils/utils";

declare var android: any; //bypass the TS warnings

let CAMERA_OPTIONS = {
    width: 2000,
    height: 1500,
    keepAspectRatio: false
};

@Component({
    selector: 'AddPost',
    moduleId: module.id,
    templateUrl: './addPost.html'
})
export class AddPostComponent implements OnInit {

    painting: any | Painting = {};

    userId;

    lat = 49.4700666;
    lng = 16.0511622;
    zoom = 11;

    marker;

    showSearch;

    imgData;

    private mapView: MapView;

    inProgress = false;

    post: User | any = {};
    postSaved = false;

    @ViewChild('postText')
    postText: ElementRef;

    @ViewChild('postImage')
    postImage: ElementRef;

    constructor(private routerExtensions: RouterExtensions,
                private paintingService: PaintingService,
                private postService: PostService,
                private userService: UserService,
                private page: Page,
                private pageRoute: PageRoute) {
    }

    ngOnInit(): void {
        this.page.on(Page.navigatedToEvent, (e: NavigatedData) => {

            this.inProgress = false;
            if (!this.userService.isLogged()) {
                dialogs.confirm({
                    title: localize("Login"),
                    message: localize("AddPostLoginMessage"),
                    okButtonText: localize("Login") + "/" + localize("Register"),
                    neutralButtonText: localize("Cancel")
                }).then((result) => {
                    console.log("Dialog result: " + result);
                    if (result) {
                        this.routerExtensions.navigate(['/register']);
                    } else {
                        this.routerExtensions.back();
                    }
                }).catch(() => this.routerExtensions.back())
            } else {
            }
        });
        this.pageRoute.activatedRoute.switchMap(r => r.params).forEach(p => this.paintingService.getPainting(+p["id"]).then(p => this.painting = p));
    }

    savePost() {
        if (this.inProgress) return;
        this.inProgress = true;
        let post = new Post();
        post.text = this.postText.nativeElement.text;
        post.userId = this.userId;
        post.paintingId = this.painting.id;
        post.lat = this.marker ? this.marker.position.latitude : null;
        post.lng = this.marker ? this.marker.position.longitude : null;

        if (this.imgData) {
            post.img = "data:image/jpeg;base64," + this.imgData;
        }
        this.postService.savePost(post).then((res) => {
            this.inProgress = false;
            Toast.makeText(localize("SavedPostSuccessfully"), 'long').show();
            prefs.setBoolean(PAINTING_SHOW_POSTS_TAB, true);
            post.tn = post.img = this.postImage.nativeElement.src;
            this.post = post;
            this.postSaved = true;
            this.post.id = res;
            console.log("Post succesfully saved :", res);
            this.userService.getCurrentUser().then(u => {
                this.post.userName = u.username;
                this.post.userId = u.id;
            });

        }).catch(err => {
            this.inProgress = false;
            console.log('failed to save post:' + JSON.stringify(err));
            Toast.makeText(localize("FailedToSavePost"), 'long').show();
        })
    }

    delete() {
        dialogs.confirm({
            title: localize("DeletePost"),
            message: localize("DeletePostMessage"),
            okButtonText: localize("Confirm"),
            neutralButtonText: localize("Cancel")
        }).then((result) => {
            if (result) {
                this.postService.deletePost(this.post.id).then(res => {
                    console.log("ADDPOST:" + res);
                    if (res.status >= 400) {
                        Toast.makeText(localize("UnableToPostDeleted"), 'long').show();
                    } else {
                        this.postSaved = false;
                        this.post = null;
                        Toast.makeText(localize("PostDeleted"), 'long').show();
                    }
                })
            }
        })
    }

    share() {
        const text =
            ( this.post.text ? this.post.text + "\n" : "") +
            this.painting.authorName + ', ' + this.painting.name + (this.painting.paintingCreated ? ', ' + this.painting.paintingCreated : '')
            + '\n' + ' #artopos #art #scenery #landscape #painting #' + this.painting.name.replace(/\s/g, "")
            + (this.painting.authorName ? " #" + this.painting.authorName.replace(/\s/g, "") : "");
        if (this.imgData) {
            clipboard.setText(text).then(() => {
                Toast.makeText(localize("TextCopiedToClipboard"), 'long').show();
            });
            SocialShare.shareImage(this.post.img, text);
        } else {
            SocialShare.shareText(text, text);
        }
    }

    addMarker() {
        this.mapView.removeAllMarkers();
        let m = new Marker();
        m.position = Position.positionFromLatLng(this.mapView.latitude, this.mapView.longitude);
        this.mapView.addMarker(m);
        this.marker = m;
    }

    clearMarker() {
        this.mapView.removeAllMarkers();
        this.marker = null;
    }

    onTabSelected(e) {
        this.showSearch = e.object.selectedIndex == 1;
        let textInput = this.postText.nativeElement;
        textInput.dismissSoftInput();
        app.android.startActivity.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private createFromAsset(imageAsset) {
        return imageSourceModule.fromAsset(imageAsset).then(res => {
            this.postImage.nativeElement.src = res;
            this.imgData = res.toBase64String("jpeg");
        })
    }

    takeImg() {
        camera.requestPermissions().then(() => {
            camera.takePicture(CAMERA_OPTIONS).then(asset => this.createFromAsset(asset))
                .catch(() => {})
        });
    }


    pickImg() {
        const context = imagepicker.create({mode: "single"});
        context.authorize()
            .then(() => context.present())
            .then((sel) => {
                sel.forEach(asset => {
                    asset.options = CAMERA_OPTIONS;
                    return this.createFromAsset(asset)
                });
            }).catch(() => {})
    }

    clearImg() {
        this.imgData = null;
        this.postImage.nativeElement.src = "res://placeholder";
    }

    onMapReady(event) {
        this.mapView = event.object;
        this.mapView.setStyle(require("~/assets/map_styles_light.json"));
        this.mapView.settings.mapToolbarEnabled = true;
        this.mapView.settings.rotateGesturesEnabled = false;
        this.mapView.settings.tiltGesturesEnabled = false;
        this.mapView.settings.zoomControlsEnabled = true;
        this.mapView.longitude = this.lng;
        this.mapView.latitude = this.lat;
        this.mapView.zoom = this.zoom;
        const boundsStr = prefs.getString(LAST_PAINTING_BOUNDS_KEY);
        if (boundsStr) {
            const b = JSON.parse(boundsStr);
            if (b.sw && b.ne) {
                this.mapView.setViewport(Bounds.fromCoordinates(
                    Position.positionFromLatLng(b.sw.lat, b.sw.lng),
                    Position.positionFromLatLng(b.ne.lat, b.ne.lng))
                );
            }
        }
        console.log("ADD  POS : " + this.mapView.latitude + " , " + this.mapView.longitude);
        geolocation.enableLocationRequest(true).then(() => {
            this.mapView.settings.myLocationButtonEnabled = true;
            this.mapView.myLocationEnabled = true;
        }).catch((er) => {
            console.log("LOCATION REJECTED : " + er);
        });
    }

    onScrollViewLoaded(event){
        const sv = event.object;
        if (sv.android) {
            const id = utils.ad.resources.getId('add_post_image_scroll_view');
            sv.android.setId(id);
        }
    }

    onPreviewScrollViewLoaded(event){
        const sv = event.object;
        if (sv.android) {
            const id = utils.ad.resources.getId('add_post_preview_scroll_view');
            sv.android.setId(id);
        }
    }
}
