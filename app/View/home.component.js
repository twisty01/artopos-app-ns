"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_bottombar_1 = require("nativescript-bottombar");
var platform = require("tns-core-modules/platform");
var homeProfile_component_1 = require("./components/homeProfile.component");
var homeList_component_1 = require("./components/homeList.component");
var homeMap_component_1 = require("./components/homeMap.component");
var page_1 = require("tns-core-modules/ui/page");
var nativescript_localize_1 = require("nativescript-localize");
var WIDTH = platform.screen.mainScreen.widthPixels;
var DURATION = 100;
var HomeComponent = (function () {
    function HomeComponent(page) {
        this.page = page;
        this.tabsRemoved = false;
        this.DEFAULT_INDEX = 1;
        this.oldIndex = this.DEFAULT_INDEX;
        this.selectedIndex = this.DEFAULT_INDEX;
        this.bottomItems = [
            new nativescript_bottombar_1.BottomBarItem(0, nativescript_localize_1.localize("List"), "ic_format_list_bulleted_white_36dp", "#222125"),
            new nativescript_bottombar_1.BottomBarItem(1, nativescript_localize_1.localize("Map"), "ic_map_white_36dp", "#2c2b30"),
            new nativescript_bottombar_1.BottomBarItem(2, nativescript_localize_1.localize("Profile"), "ic_account_white_36dp", "#3a3a40"),
        ];
    }
    HomeComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.tabContents = [
            this.list,
            this.map,
            this.profile
        ];
        this.page.on(page_1.Page.navigatedToEvent, function () {
            _this.itemCallbacks(_this.selectedIndex, _this.oldIndex);
        });
    };
    HomeComponent.prototype.tabsLoaded = function (event) {
        this.tv = this.homeTabView.nativeElement;
        if (!this.tv)
            return;
        this.tv.selectedIndex = this.selectedIndex;
        if (this.tabsRemoved)
            return;
        this.tabsRemoved = true;
        if (this.tv.android) {
            this.tv.android.removeViewAt(0);
        }
        else {
            this.tv.ios.tabBar.hidden = true;
        }
    };
    HomeComponent.prototype.indexChanged = function (event) {
        if (!this.tv)
            return;
        this.selectedIndex = this.tv.selectedIndex;
        if (this._bar)
            this._bar.selectItem(this.selectedIndex);
        console.log("HOME INDEX CHANGED: " + this.tv.selectedIndex);
    };
    HomeComponent.prototype.bottomBarLoaded = function (event) {
        this._bar = event.object;
        this.titleState = 0 /* SHOW_WHEN_ACTIVE */;
        this.inactiveColor = "white";
        this.accentColor = "#d84e4d";
        this._bar.selectItem(this.selectedIndex);
    };
    HomeComponent.prototype.bottomBarSelected = function (event) {
        if (!this.tv)
            return;
        if (event.newIndex != this.tv.selectedIndex) {
            this.itemCallbacks(event.newIndex, event.oldIndex);
        }
        this.tv.selectedIndex = event.newIndex;
    };
    HomeComponent.prototype.itemCallbacks = function (newIndex, oldIndex) {
        if (newIndex != oldIndex && this.tabContents) {
            this.oldIndex = oldIndex;
            if (this.tabContents[oldIndex] && this.tabContents[oldIndex].tabDeselected)
                this.tabContents[oldIndex].tabDeselected(this);
            if (this.tabContents[newIndex] && this.tabContents[newIndex].tabSelected)
                this.tabContents[newIndex].tabSelected(this);
        }
    };
    HomeComponent.prototype.changeIndex = function (i) {
        if (i === void 0) { i = null; }
        if (i === null)
            i = this.oldIndex;
        if (!this.tv)
            return;
        console.log("HOME CHANGE INDEX : " + i);
        this.tv.selectedIndex = i;
    };
    __decorate([
        core_1.ViewChild('homeTabView'),
        __metadata("design:type", core_1.ElementRef)
    ], HomeComponent.prototype, "homeTabView", void 0);
    __decorate([
        core_1.ViewChild(homeMap_component_1.HomeMapComponent),
        __metadata("design:type", homeMap_component_1.HomeMapComponent)
    ], HomeComponent.prototype, "map", void 0);
    __decorate([
        core_1.ViewChild(homeList_component_1.HomeListComponent),
        __metadata("design:type", homeList_component_1.HomeListComponent)
    ], HomeComponent.prototype, "list", void 0);
    __decorate([
        core_1.ViewChild(homeProfile_component_1.HomeProfileComponent),
        __metadata("design:type", homeProfile_component_1.HomeProfileComponent)
    ], HomeComponent.prototype, "profile", void 0);
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.html"
        }),
        __metadata("design:paramtypes", [page_1.Page])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
