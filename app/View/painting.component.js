"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_angular_1 = require("nativescript-angular");
var router_1 = require("nativescript-angular/router");
require("rxjs/add/operator/switchMap");
var nativescript_google_maps_sdk_1 = require("nativescript-google-maps-sdk");
var geolocation = require("nativescript-geolocation");
var user_service_1 = require("../Service/user.service");
var painting_service_1 = require("../Service/painting.service");
var post_service_1 = require("../Service/post.service");
var prefs = require("tns-core-modules/application-settings");
var SocialShare = require("nativescript-social-share");
var imageSourceModule = require("tns-core-modules/image-source");
var clipboard = require("nativescript-clipboard");
var Toast = require("nativescript-toast");
var nativescript_localize_1 = require("nativescript-localize");
var page_1 = require("tns-core-modules/ui/page");
var utils = require("tns-core-modules/utils/utils");
exports.LAST_PAINTING_BOUNDS_KEY = 'last_painting_bounds';
exports.PAINTING_POST_IMGS = 'painting_post_imgs';
exports.PAINTING_POST_INFO = 'painting_post_info';
exports.PAINTING_SHOW_POSTS_TAB = 'show_posts_tab';
var PaintingComponent = (function () {
    function PaintingComponent(routerExtensions, paintingService, postService, userService, page, pageRoute) {
        this.routerExtensions = routerExtensions;
        this.paintingService = paintingService;
        this.postService = postService;
        this.userService = userService;
        this.page = page;
        this.pageRoute = pageRoute;
        this.lat = 49.4700666;
        this.lng = 16.0511622;
        this.zoom = 11;
        this.painting = {};
        this.posts = [];
        this.postsReady = false;
        this.selectedIndex = 0;
        this.loadingPosts = false;
    }
    PaintingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pageRoute.activatedRoute.switchMap(function (r) { return r.params; }).forEach(function (p) { return _this.loadData(+p["id"]); });
        this.page.on(page_1.Page.navigatingToEvent, function () {
            if (_this.painting && _this.painting.id) {
                _this.loadPosts(_this.painting.id);
                if (prefs.hasKey(exports.PAINTING_SHOW_POSTS_TAB)) {
                    _this.selectedIndex = 2;
                    prefs.remove(exports.PAINTING_SHOW_POSTS_TAB);
                }
            }
        });
    };
    PaintingComponent.prototype.addPaintingMarker = function (painting) {
        var m = new nativescript_google_maps_sdk_1.Marker();
        m.color = painting.resolved && painting.resolved != "false" ? "green" : "red";
        m.position = nativescript_google_maps_sdk_1.Position.positionFromLatLng(painting.lat, painting.lng);
        m.title = painting.name;
        m.snippet = painting.authorName;
        this.mapView.addMarker(m);
    };
    PaintingComponent.prototype.addPostMarker = function (post) {
        var m = new nativescript_google_maps_sdk_1.Marker();
        m.color = post.resolved && post.resolved != "false" ? "green" : "red";
        m.alpha = 0.2;
        m.position = nativescript_google_maps_sdk_1.Position.positionFromLatLng(post.lat, post.lng);
        m.title = post.userName;
        // if (post.created) m.snippet = post.created;
        this.mapView.addMarker(m);
    };
    PaintingComponent.prototype.updatePosts = function (posts) {
        var _this = this;
        this.postsReady = false;
        var vp = this.mapView.projection.visibleRegion.bounds;
        this.mapView.removeAllMarkers();
        this.addPaintingMarker(this.painting);
        posts.forEach(function (p) {
            _this.addPostMarker(p);
            // if ( this.painting.resolved && p.resolved ){
            if (p.lat < vp.southwest.latitude) {
                vp.southwest.latitude = p.lat;
            }
            if (p.lat > vp.northeast.latitude) {
                vp.northeast.latitude = p.lat;
            }
            if (p.lat < vp.southwest.longitude) {
                vp.southwest.longitude = p.lng;
            }
            if (p.lat > vp.northeast.longitude) {
                vp.northeast.longitude = p.lng;
            }
            // }
        });
        //this.mapView.setViewport(vp);
    };
    PaintingComponent.prototype.loadPosts = function (id) {
        var _this = this;
        this.loadingPosts = true;
        this.postService.getPostsByPainting(id)
            .then(function (posts) {
            _this.posts = posts.reverse();
            _this.loadingPosts = false;
            if (!_this.mapView) {
                _this.postsReady = true;
                return;
            }
            _this.updatePosts(posts);
        }).catch(function (err) {
            _this.loadingPosts = false;
            console.log("Unable to load posts :  " + JSON.stringify(err));
        });
    };
    PaintingComponent.prototype.loadData = function (id) {
        var _this = this;
        this.paintingService.getPainting(id).then(function (painting) {
            if (!painting)
                _this.routerExtensions.back();
            _this.lat = painting.lat;
            _this.lng = painting.lng;
            if (_this.mapView) {
                _this.addPaintingMarker(painting);
                _this.mapView.longitude = _this.lng;
                _this.mapView.latitude = _this.lat;
                _this.mapView.zoom = _this.zoom;
            }
            return _this.painting = painting;
        }).then(function (_a) {
            var id = _a.id;
            return _this.loadPosts(id);
        });
    };
    PaintingComponent.prototype.onMapReady = function (event) {
        var _this = this;
        this.mapView = event.object;
        this.mapView.setStyle(require("~/assets/map_styles_light.json"));
        this.mapView.settings.mapToolbarEnabled = true;
        this.mapView.settings.rotateGesturesEnabled = false;
        this.mapView.settings.tiltGesturesEnabled = false;
        this.mapView.settings.scrollGesturesEnabled = true;
        this.mapView.settings.zoomControlsEnabled = true;
        geolocation.enableLocationRequest(true).then(function () {
            _this.mapView.settings.myLocationButtonEnabled = true;
            _this.mapView.myLocationEnabled = true;
        }).catch(function (er) {
            console.log("LOCATION REJECTED : " + er);
        });
        if (this.painting) {
            this.addPaintingMarker(this.painting);
            this.lat = this.painting.lat;
            this.lng = this.painting.lng;
            this.mapView.longitude = this.lng;
            this.mapView.latitude = this.lat;
            this.mapView.zoom = this.zoom;
        }
        if (this.postsReady) {
            this.updatePosts(this.posts);
        }
    };
    PaintingComponent.prototype.addPost = function () {
        var b = this.mapView.projection.visibleRegion.bounds;
        prefs.setString(exports.LAST_PAINTING_BOUNDS_KEY, JSON.stringify({
            sw: { lat: b.southwest.latitude, lng: b.southwest.longitude },
            ne: { lat: b.northeast.latitude, lng: b.northeast.longitude }
        }));
        this.routerExtensions.navigate(["/addPost", this.painting.id], {});
    };
    PaintingComponent.prototype.onInfoScroll = function (event, scrollView, topView) {
        if (scrollView.verticalOffset < 400) {
            var offset = scrollView.verticalOffset / 2;
            if (scrollView.ios) {
                // iOS adjust the position with an animation to create a smother scrolling effect. 
                topView.animate({ translate: { x: 0, y: offset } });
            }
            else {
                // Android, animations are jerky so instead just adjust the position without animation.
                topView.translateY = Math.floor(offset);
            }
        }
    };
    PaintingComponent.prototype.onPostItemTap = function (args) {
        prefs.remove(exports.PAINTING_POST_IMGS);
        if (args) {
            var post = this.posts[args.index];
            if (post.img) {
                prefs.setString(exports.PAINTING_POST_IMGS, post.img);
                prefs.setString(exports.PAINTING_POST_INFO, post.userName + ", " + post.created);
            }
        }
        this.routerExtensions.navigate(["/painting/image", this.painting.id]);
    };
    PaintingComponent.prototype.sharePainting = function () {
        var _this = this;
        imageSourceModule.fromUrl(this.painting.img).then(function (imgSrc) {
            var text = _this.painting.authorName +
                ', ' + _this.painting.name +
                (_this.painting.paintingCreated ? ', ' + _this.painting.paintingCreated : '') + '\n' +
                ' #artopos #art #scenery #landscape #painting';
            clipboard.setText(text).then(function () {
                Toast.makeText(nativescript_localize_1.localize("TextCopiedToClipboard"), 'long').show();
            });
            SocialShare.shareImage(imgSrc, text);
        });
    };
    PaintingComponent.prototype.onListViewLoaded = function (event) {
        var lv = event.object;
        if (lv.android) {
            var id = utils.ad.resources.getId('detail_post_list_view');
            lv.android.setId(id);
        }
    };
    PaintingComponent.prototype.onScrollViewLoaded = function (event) {
        var sv = event.object;
        if (sv.android) {
            var id = utils.ad.resources.getId('detail_info_scroll_view');
            sv.android.setId(id);
        }
    };
    PaintingComponent = __decorate([
        core_1.Component({
            selector: "PaintingDetail",
            moduleId: module.id,
            templateUrl: "./painting.html"
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions,
            painting_service_1.PaintingService,
            post_service_1.PostService,
            user_service_1.UserService,
            page_1.Page,
            router_1.PageRoute])
    ], PaintingComponent);
    return PaintingComponent;
}());
exports.PaintingComponent = PaintingComponent;
