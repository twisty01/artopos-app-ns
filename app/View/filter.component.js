"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var homeList_component_1 = require("./components/homeList.component");
var nativescript_angular_1 = require("nativescript-angular");
var page_1 = require("tns-core-modules/ui/page");
var prefs = require("tns-core-modules/application-settings");
var debounce_1 = require("../Helpers/debounce");
var geocoding_service_1 = require("../Service/geocoding.service");
var nativescript_localize_1 = require("nativescript-localize");
var api_service_1 = require("../Service/api.service");
var FilterComponent = (function () {
    function FilterComponent(routerExtensions, page, geocoding, api, cdr) {
        var _this = this;
        this.routerExtensions = routerExtensions;
        this.page = page;
        this.geocoding = geocoding;
        this.api = api;
        this.cdr = cdr;
        this.filterOptions = {};
        this.resolvedOptions = [
            nativescript_localize_1.localize('None'),
            nativescript_localize_1.localize('Resolved'),
            nativescript_localize_1.localize('NotResolved'),
        ];
        this.institutions = [];
        // search
        this.searchItems = [];
        this.resIndex = 0;
        this.instIndex = 0;
        // Search
        this.search = debounce_1.debounce(function (args) {
            if (args === void 0) { args = null; }
            var txt = _this.searchInput.nativeElement.text;
            if (txt.length < 3)
                return;
            _this.geocoding.getPlaces(txt).then(function (results) {
                _this.searchItems = results;
            });
        }, 700);
        this.page.on(page_1.Page.navigatingFromEvent, function (e) {
            var insts = _this.institutionPicker.nativeElement;
            if (!insts.selectedIndex) {
                _this.filterOptions.institutionName = null;
            }
            else {
                _this.filterOptions.institutionName = _this.institutions[insts.selectedIndex];
            }
            var rslvd = _this.resolvedPicker.nativeElement;
            switch (rslvd.selectedIndex) {
                case 0:
                    _this.filterOptions.resolved = null;
                    break;
                case 1:
                    _this.filterOptions.resolved = true;
                    break;
                case 2:
                    _this.filterOptions.resolved = false;
                    break;
            }
            prefs.setString(homeList_component_1.FILTER_OPTIONS_KEY, JSON.stringify(_this.filterOptions));
        });
        this.api.get("/institutions").then(function (r) {
            _this.institutions = [nativescript_localize_1.localize('None')];
            r.forEach(function (ins, i) {
                _this.institutions.push(ins.name);
                if (ins.name == _this.filterOptions.institutionName) {
                    _this.instIndex = i + 1;
                }
            });
            _this.cdr.detectChanges();
        });
        this.page.on(page_1.Page.navigatedToEvent, function (e) {
            var options = prefs.getString(homeList_component_1.FILTER_OPTIONS_KEY);
            if (options) {
                _this.filterOptions = JSON.parse(options);
            }
            else {
                return;
            }
            if (_this.filterOptions.resolved === true) {
                _this.resIndex = 1;
            }
            else if (_this.filterOptions.resolved === false) {
                _this.resIndex = 2;
            }
            else {
                _this.resIndex = 0;
            }
            if (_this.institutions) {
                _this.institutions.forEach(function (ins, i) {
                    if (ins.name == _this.filterOptions.institutionName) {
                        _this.instIndex = i + 1;
                    }
                });
            }
            _this.cdr.detectChanges();
        });
    }
    FilterComponent.prototype.removeFilters = function () {
        this.instIndex = 0;
        this.resIndex = 0;
        this.clearAddress();
        this.filterOptions = {};
    };
    FilterComponent.prototype.resultClick = function (placeItem) {
        var _this = this;
        if (!placeItem)
            return;
        this.geocoding.getPlace(placeItem).then(function (place) {
            _this.filterOptions.address = place.data.result.formatted_address;
            var bounds = place.data.result.geometry.viewport;
            _this.filterOptions.bounds = {
                southwest: {
                    latitude: bounds.southwest.lat,
                    longitude: bounds.southwest.lng
                },
                northeast: {
                    latitude: bounds.northeast.lat,
                    longitude: bounds.northeast.lng
                }
            };
            _this.searchItems = [];
            _this.searchInput.nativeElement.text = "";
        });
    };
    FilterComponent.prototype.onItemTap = function ($event) {
        this.resultClick(this.searchItems[$event.index]);
    };
    FilterComponent.prototype.clearSearch = function () {
        this.searchItems = [];
        this.searchInput.nativeElement.text = "";
    };
    FilterComponent.prototype.clearAddress = function () {
        this.filterOptions.bounds = null;
        this.filterOptions.address = "";
    };
    __decorate([
        core_1.ViewChild('resolvedPicker'),
        __metadata("design:type", core_1.ElementRef)
    ], FilterComponent.prototype, "resolvedPicker", void 0);
    __decorate([
        core_1.ViewChild('institutionPicker'),
        __metadata("design:type", core_1.ElementRef)
    ], FilterComponent.prototype, "institutionPicker", void 0);
    __decorate([
        core_1.ViewChild('searchInput'),
        __metadata("design:type", core_1.ElementRef)
    ], FilterComponent.prototype, "searchInput", void 0);
    FilterComponent = __decorate([
        core_1.Component({
            selector: "Filter",
            moduleId: module.id,
            templateUrl: "./filter.html",
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions, page_1.Page, geocoding_service_1.GeocodingService, api_service_1.ApiService, core_1.ChangeDetectorRef])
    ], FilterComponent);
    return FilterComponent;
}());
exports.FilterComponent = FilterComponent;
