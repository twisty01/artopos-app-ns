"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_angular_1 = require("nativescript-angular");
var user_service_1 = require("../Service/user.service");
var post_service_1 = require("../Service/post.service");
var painting_service_1 = require("../Service/painting.service");
var post_1 = require("../Model/post");
var geolocation = require("nativescript-geolocation");
var page_1 = require("tns-core-modules/ui/page/page");
var nativescript_google_maps_sdk_1 = require("nativescript-google-maps-sdk");
var dialogs = require("tns-core-modules/ui/dialogs");
var nativescript_localize_1 = require("nativescript-localize");
var imagepicker = require("nativescript-imagepicker");
var camera = require("nativescript-camera");
var imageSourceModule = require("image-source");
var prefs = require("tns-core-modules/application-settings");
var painting_component_1 = require("./painting.component");
var Toast = require("nativescript-toast");
var app = require("tns-core-modules/application");
var SocialShare = require("nativescript-social-share");
var clipboard = require("nativescript-clipboard");
var utils = require("tns-core-modules/utils/utils");
var CAMERA_OPTIONS = {
    width: 2000,
    height: 1500,
    keepAspectRatio: false
};
var AddPostComponent = (function () {
    function AddPostComponent(routerExtensions, paintingService, postService, userService, page, pageRoute) {
        this.routerExtensions = routerExtensions;
        this.paintingService = paintingService;
        this.postService = postService;
        this.userService = userService;
        this.page = page;
        this.pageRoute = pageRoute;
        this.painting = {};
        this.lat = 49.4700666;
        this.lng = 16.0511622;
        this.zoom = 11;
        this.inProgress = false;
        this.post = {};
        this.postSaved = false;
    }
    AddPostComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.on(page_1.Page.navigatedToEvent, function (e) {
            _this.inProgress = false;
            if (!_this.userService.isLogged()) {
                dialogs.confirm({
                    title: nativescript_localize_1.localize("Login"),
                    message: nativescript_localize_1.localize("AddPostLoginMessage"),
                    okButtonText: nativescript_localize_1.localize("Login") + "/" + nativescript_localize_1.localize("Register"),
                    neutralButtonText: nativescript_localize_1.localize("Cancel")
                }).then(function (result) {
                    console.log("Dialog result: " + result);
                    if (result) {
                        _this.routerExtensions.navigate(['/register']);
                    }
                    else {
                        _this.routerExtensions.back();
                    }
                }).catch(function () { return _this.routerExtensions.back(); });
            }
            else {
            }
        });
        this.pageRoute.activatedRoute.switchMap(function (r) { return r.params; }).forEach(function (p) { return _this.paintingService.getPainting(+p["id"]).then(function (p) { return _this.painting = p; }); });
    };
    AddPostComponent.prototype.savePost = function () {
        var _this = this;
        if (this.inProgress)
            return;
        this.inProgress = true;
        var post = new post_1.Post();
        post.text = this.postText.nativeElement.text;
        post.userId = this.userId;
        post.paintingId = this.painting.id;
        post.lat = this.marker ? this.marker.position.latitude : null;
        post.lng = this.marker ? this.marker.position.longitude : null;
        if (this.imgData) {
            post.img = "data:image/jpeg;base64," + this.imgData;
        }
        this.postService.savePost(post).then(function (res) {
            _this.inProgress = false;
            Toast.makeText(nativescript_localize_1.localize("SavedPostSuccessfully"), 'long').show();
            prefs.setBoolean(painting_component_1.PAINTING_SHOW_POSTS_TAB, true);
            post.tn = post.img = _this.postImage.nativeElement.src;
            _this.post = post;
            _this.postSaved = true;
            _this.post.id = res;
            console.log("Post succesfully saved :", res);
            _this.userService.getCurrentUser().then(function (u) {
                _this.post.userName = u.username;
                _this.post.userId = u.id;
            });
        }).catch(function (err) {
            _this.inProgress = false;
            console.log('failed to save post:' + JSON.stringify(err));
            Toast.makeText(nativescript_localize_1.localize("FailedToSavePost"), 'long').show();
        });
    };
    AddPostComponent.prototype.delete = function () {
        var _this = this;
        dialogs.confirm({
            title: nativescript_localize_1.localize("DeletePost"),
            message: nativescript_localize_1.localize("DeletePostMessage"),
            okButtonText: nativescript_localize_1.localize("Confirm"),
            neutralButtonText: nativescript_localize_1.localize("Cancel")
        }).then(function (result) {
            if (result) {
                _this.postService.deletePost(_this.post.id).then(function (res) {
                    console.log("ADDPOST:" + res);
                    if (res.status >= 400) {
                        Toast.makeText(nativescript_localize_1.localize("UnableToPostDeleted"), 'long').show();
                    }
                    else {
                        _this.postSaved = false;
                        _this.post = null;
                        Toast.makeText(nativescript_localize_1.localize("PostDeleted"), 'long').show();
                    }
                });
            }
        });
    };
    AddPostComponent.prototype.share = function () {
        var text = (this.post.text ? this.post.text + "\n" : "") +
            this.painting.authorName + ', ' + this.painting.name + (this.painting.paintingCreated ? ', ' + this.painting.paintingCreated : '')
            + '\n' + ' #artopos #art #scenery #landscape #painting #' + this.painting.name.replace(/\s/g, "")
            + (this.painting.authorName ? " #" + this.painting.authorName.replace(/\s/g, "") : "");
        if (this.imgData) {
            clipboard.setText(text).then(function () {
                Toast.makeText(nativescript_localize_1.localize("TextCopiedToClipboard"), 'long').show();
            });
            SocialShare.shareImage(this.post.img, text);
        }
        else {
            SocialShare.shareText(text, text);
        }
    };
    AddPostComponent.prototype.addMarker = function () {
        this.mapView.removeAllMarkers();
        var m = new nativescript_google_maps_sdk_1.Marker();
        m.position = nativescript_google_maps_sdk_1.Position.positionFromLatLng(this.mapView.latitude, this.mapView.longitude);
        this.mapView.addMarker(m);
        this.marker = m;
    };
    AddPostComponent.prototype.clearMarker = function () {
        this.mapView.removeAllMarkers();
        this.marker = null;
    };
    AddPostComponent.prototype.onTabSelected = function (e) {
        this.showSearch = e.object.selectedIndex == 1;
        var textInput = this.postText.nativeElement;
        textInput.dismissSoftInput();
        app.android.startActivity.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    };
    AddPostComponent.prototype.createFromAsset = function (imageAsset) {
        var _this = this;
        return imageSourceModule.fromAsset(imageAsset).then(function (res) {
            _this.postImage.nativeElement.src = res;
            _this.imgData = res.toBase64String("jpeg");
        });
    };
    AddPostComponent.prototype.takeImg = function () {
        var _this = this;
        camera.requestPermissions().then(function () {
            camera.takePicture(CAMERA_OPTIONS).then(function (asset) { return _this.createFromAsset(asset); })
                .catch(function () { });
        });
    };
    AddPostComponent.prototype.pickImg = function () {
        var _this = this;
        var context = imagepicker.create({ mode: "single" });
        context.authorize()
            .then(function () { return context.present(); })
            .then(function (sel) {
            sel.forEach(function (asset) {
                asset.options = CAMERA_OPTIONS;
                return _this.createFromAsset(asset);
            });
        }).catch(function () { });
    };
    AddPostComponent.prototype.clearImg = function () {
        this.imgData = null;
        this.postImage.nativeElement.src = "res://placeholder";
    };
    AddPostComponent.prototype.onMapReady = function (event) {
        var _this = this;
        this.mapView = event.object;
        this.mapView.setStyle(require("~/assets/map_styles_light.json"));
        this.mapView.settings.mapToolbarEnabled = true;
        this.mapView.settings.rotateGesturesEnabled = false;
        this.mapView.settings.tiltGesturesEnabled = false;
        this.mapView.settings.zoomControlsEnabled = true;
        this.mapView.longitude = this.lng;
        this.mapView.latitude = this.lat;
        this.mapView.zoom = this.zoom;
        var boundsStr = prefs.getString(painting_component_1.LAST_PAINTING_BOUNDS_KEY);
        if (boundsStr) {
            var b = JSON.parse(boundsStr);
            if (b.sw && b.ne) {
                this.mapView.setViewport(nativescript_google_maps_sdk_1.Bounds.fromCoordinates(nativescript_google_maps_sdk_1.Position.positionFromLatLng(b.sw.lat, b.sw.lng), nativescript_google_maps_sdk_1.Position.positionFromLatLng(b.ne.lat, b.ne.lng)));
            }
        }
        console.log("ADD  POS : " + this.mapView.latitude + " , " + this.mapView.longitude);
        geolocation.enableLocationRequest(true).then(function () {
            _this.mapView.settings.myLocationButtonEnabled = true;
            _this.mapView.myLocationEnabled = true;
        }).catch(function (er) {
            console.log("LOCATION REJECTED : " + er);
        });
    };
    AddPostComponent.prototype.onScrollViewLoaded = function (event) {
        var sv = event.object;
        if (sv.android) {
            var id = utils.ad.resources.getId('add_post_image_scroll_view');
            sv.android.setId(id);
        }
    };
    AddPostComponent.prototype.onPreviewScrollViewLoaded = function (event) {
        var sv = event.object;
        if (sv.android) {
            var id = utils.ad.resources.getId('add_post_preview_scroll_view');
            sv.android.setId(id);
        }
    };
    __decorate([
        core_1.ViewChild('postText'),
        __metadata("design:type", core_1.ElementRef)
    ], AddPostComponent.prototype, "postText", void 0);
    __decorate([
        core_1.ViewChild('postImage'),
        __metadata("design:type", core_1.ElementRef)
    ], AddPostComponent.prototype, "postImage", void 0);
    AddPostComponent = __decorate([
        core_1.Component({
            selector: 'AddPost',
            moduleId: module.id,
            templateUrl: './addPost.html'
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions,
            painting_service_1.PaintingService,
            post_service_1.PostService,
            user_service_1.UserService,
            page_1.Page,
            nativescript_angular_1.PageRoute])
    ], AddPostComponent);
    return AddPostComponent;
}());
exports.AddPostComponent = AddPostComponent;
