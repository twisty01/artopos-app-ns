import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {RouterExtensions} from "nativescript-angular";
import {PageRoute} from "nativescript-angular/router";
import "rxjs/add/operator/switchMap";
import {ScrollEventData, ScrollView} from "ui/scroll-view";
import {View} from "tns-core-modules/ui/core/view/view";
import {MapView, Marker, Position} from "nativescript-google-maps-sdk";
import * as geolocation from "nativescript-geolocation";
import {UserService} from "../Service/user.service";
import {PaintingService} from "../Service/painting.service";
import {PostService} from "../Service/post.service";
import {Painting} from "../Model/painting";
import {Post} from "../Model/post";
import * as prefs from "tns-core-modules/application-settings";
import * as SocialShare from "nativescript-social-share";
import * as imageSourceModule from "tns-core-modules/image-source";
import * as clipboard from "nativescript-clipboard";
import * as Toast from "nativescript-toast";
import {localize} from "nativescript-localize";
import {Page} from "tns-core-modules/ui/page";
import * as utils from "tns-core-modules/utils/utils";

export const LAST_PAINTING_BOUNDS_KEY = 'last_painting_bounds';
export const PAINTING_POST_IMGS = 'painting_post_imgs';
export const PAINTING_POST_INFO = 'painting_post_info';
export const PAINTING_SHOW_POSTS_TAB = 'show_posts_tab';

@Component({
    selector: "PaintingDetail",
    moduleId: module.id,
    templateUrl: "./painting.html"
})
export class PaintingComponent implements OnInit {
    lat = 49.4700666;
    lng = 16.0511622;
    zoom = 11;

    painting: Painting | any = {};
    posts: Array<Post> = [];

    private mapView: MapView;
    private postsReady = false;

    selectedIndex = 0;

    loadingPosts = false;

    constructor(private routerExtensions: RouterExtensions,
                private paintingService: PaintingService,
                private postService: PostService,
                private userService: UserService,
                private page: Page,
                private pageRoute: PageRoute) {}

    ngOnInit(): void {
        this.pageRoute.activatedRoute.switchMap(r => r.params).forEach(p => this.loadData(+p["id"]));
        this.page.on(Page.navigatingToEvent, () => {
            if (this.painting && this.painting.id) {
                this.loadPosts(this.painting.id);
                if (prefs.hasKey(PAINTING_SHOW_POSTS_TAB)) {
                    this.selectedIndex = 2;
                    prefs.remove(PAINTING_SHOW_POSTS_TAB);
                }
            }
        })
    }

    private addPaintingMarker(painting) {
        let m = new Marker();
        m.color = painting.resolved && painting.resolved != "false" ? "green" : "red";
        m.position = Position.positionFromLatLng(painting.lat, painting.lng);
        m.title = painting.name;
        m.snippet = painting.authorName;
        this.mapView.addMarker(m);
    }

    private addPostMarker(post) {
        let m = new Marker();
        m.color = post.resolved && post.resolved != "false" ? "green" : "red";
        m.alpha = 0.2;
        m.position = Position.positionFromLatLng(post.lat, post.lng);
        m.title = post.userName;
        // if (post.created) m.snippet = post.created;
        this.mapView.addMarker(m);
    }

    updatePosts(posts) {
        this.postsReady = false;
        let vp = this.mapView.projection.visibleRegion.bounds;
        this.mapView.removeAllMarkers();
        this.addPaintingMarker(this.painting);
        posts.forEach(p => {
            this.addPostMarker(p);
            // if ( this.painting.resolved && p.resolved ){
            if (p.lat < vp.southwest.latitude) {
                vp.southwest.latitude = p.lat;
            }
            if (p.lat > vp.northeast.latitude) {
                vp.northeast.latitude = p.lat;
            }
            if (p.lat < vp.southwest.longitude) {
                vp.southwest.longitude = p.lng;
            }
            if (p.lat > vp.northeast.longitude) {
                vp.northeast.longitude = p.lng;
            }
            // }
        });
        //this.mapView.setViewport(vp);
    }

    private loadPosts(id) {
        this.loadingPosts = true;
        this.postService.getPostsByPainting(id)
            .then((posts: Array<Post>) => {
                this.posts = posts.reverse();
                this.loadingPosts = false;
                if (!this.mapView) {
                    this.postsReady = true;
                    return;
                }
                this.updatePosts(posts)
            }).catch(err => {
            this.loadingPosts = false;
            console.log("Unable to load posts :  " + JSON.stringify(err));
        });
    }

    loadData(id) {
        this.paintingService.getPainting(id).then((painting) => {
            if (!painting) this.routerExtensions.back();
            this.lat = painting.lat;
            this.lng = painting.lng;
            if (this.mapView) {
                this.addPaintingMarker(painting);
                this.mapView.longitude = this.lng;
                this.mapView.latitude = this.lat;
                this.mapView.zoom = this.zoom;
            }
            return this.painting = painting;
        }).then(({id}) => this.loadPosts(id));
    }

    onMapReady(event) {
        this.mapView = event.object;
        this.mapView.setStyle(require("~/assets/map_styles_light.json"));
        this.mapView.settings.mapToolbarEnabled = true;
        this.mapView.settings.rotateGesturesEnabled = false;
        this.mapView.settings.tiltGesturesEnabled = false;
        this.mapView.settings.scrollGesturesEnabled = true;
        this.mapView.settings.zoomControlsEnabled = true;
        geolocation.enableLocationRequest(true).then(() => {
            this.mapView.settings.myLocationButtonEnabled = true;
            this.mapView.myLocationEnabled = true;
        }).catch((er) => {
            console.log("LOCATION REJECTED : " + er);
        });
        if (this.painting) {
            this.addPaintingMarker(this.painting);
            this.lat = this.painting.lat;
            this.lng = this.painting.lng;
            this.mapView.longitude = this.lng;
            this.mapView.latitude = this.lat;
            this.mapView.zoom = this.zoom;
        }
        if (this.postsReady) {
            this.updatePosts(this.posts);
        }
    }

    addPost() {
        const b = this.mapView.projection.visibleRegion.bounds;
        prefs.setString(LAST_PAINTING_BOUNDS_KEY, JSON.stringify({
            sw: {lat: b.southwest.latitude, lng: b.southwest.longitude},
            ne: {lat: b.northeast.latitude, lng: b.northeast.longitude}
        }));
        this.routerExtensions.navigate(["/addPost", this.painting.id], {});
    }

    onInfoScroll(event: ScrollEventData, scrollView: ScrollView, topView: View) {
        if (scrollView.verticalOffset < 400) {
            const offset = scrollView.verticalOffset / 2;
            if (scrollView.ios) {
                // iOS adjust the position with an animation to create a smother scrolling effect. 
                topView.animate({translate: {x: 0, y: offset}})
            } else {
                // Android, animations are jerky so instead just adjust the position without animation.
                topView.translateY = Math.floor(offset);
            }
        }
    }

    onPostItemTap(args) {
        prefs.remove(PAINTING_POST_IMGS);
        if (args) {
            const post = this.posts[args.index];
            if (post.img) {
                prefs.setString(PAINTING_POST_IMGS, post.img);
                prefs.setString(PAINTING_POST_INFO, post.userName + ", " + post.created);
            }
        }
        this.routerExtensions.navigate(["/painting/image", this.painting.id]);
    }

    sharePainting() {
        imageSourceModule.fromUrl(this.painting.img).then((imgSrc) => {
            const text =
                this.painting.authorName +
                ', ' + this.painting.name +
                (this.painting.paintingCreated ? ', ' + this.painting.paintingCreated : '') + '\n' +
                ' #artopos #art #scenery #landscape #painting';

            clipboard.setText(text).then(() => {
                Toast.makeText(localize("TextCopiedToClipboard"), 'long').show();
            });
            SocialShare.shareImage(imgSrc, text);
        });
    }


    onListViewLoaded(event) {
        const lv = event.object;
        if (lv.android) {
            const id = utils.ad.resources.getId('detail_post_list_view');
            lv.android.setId(id);
        }
    }


    onScrollViewLoaded(event) {
        const sv = event.object;
        if (sv.android) {
            const id = utils.ad.resources.getId('detail_info_scroll_view');
            sv.android.setId(id);
        }
    }

}