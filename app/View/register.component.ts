import {ChangeDetectorRef, Component, ElementRef, ViewChild} from "@angular/core";
import {RouterExtensions} from "nativescript-angular";
import {UserService} from "../Service/user.service";
import {localize} from "nativescript-localize";
import * as Toast from "nativescript-toast";
import {Page} from "tns-core-modules/ui/page";

@Component({
    selector: "Register",
    moduleId: module.id,
    templateUrl: "./register.html",
})
export class RegisterComponent {

    @ViewChild('email') emailRef: ElementRef;
    @ViewChild('username') usernameRef: ElementRef;
    @ViewChild('password') passwordRef: ElementRef;
    @ViewChild('passwordRepeat') passwordRepeatRef: ElementRef;

    register: boolean;

    error: string = "";
    username: string = "";
    email: string = "";
    password: string = "";
    passwordRepeat: string = "";

    private inProgress = false;

    constructor(private routerExtensions: RouterExtensions,
                private userService: UserService,
                private page: Page, private cdr: ChangeDetectorRef) {
        this.page.on(Page.navigatedToEvent, () => {
            this.inProgress = false;
            this.register = false;
            this.cdr.detectChanges();
        })
    }

    private validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    submit(): void {
        if ( this.inProgress ) return;
        this.password = this.passwordRef.nativeElement.text;
        this.email =    this.emailRef.nativeElement.text;
        this.error = "";
        let invalid = false;
        if (!this.validateEmail(this.email)) {
            invalid = true;
            this.error += localize("InvalidEmail") + "\n";
        }
        if (!this.password || this.password.length < 6) {
            invalid = true;
            this.error += localize("InvalidPassword") + "\n";
        }
        if (this.register) {
            this.passwordRepeat =   this.passwordRepeatRef.nativeElement.text;
            this.username =         this.usernameRef.nativeElement.text;

            if (this.password != this.passwordRepeat) {
                invalid = true;
                this.error += localize("InvalidPassword") + "\n";
            }
            if (!this.username) {
                invalid = true;
                this.error += localize("InvalidUsername") + "\n";
            }
            if (invalid) return;
            this.inProgress = true;
            this.userService.register(this.username, this.email, this.passwordRepeat).then(u => {
                Toast.makeText(localize("RegistrationWasSuccessful"), 'long').show();
                this.routerExtensions.back();
                this.inProgress = false;
            }).catch(err => {
                console.log("Error register:" + JSON.stringify(err));
                Toast.makeText(localize("ErrorRegister"), 'long').show();
                this.error += localize("ErrorRegister");
                this.inProgress = false;
            })
        } else {
            // LOGIN
            if (invalid) return;
            this.inProgress = true;
            this.userService.tryLogin(this.email, this.password).then((u) => {
                Toast.makeText(localize("LoginWasSuccessful"), 'long').show();
                this.routerExtensions.back();
                this.inProgress = false;
            }).catch((err) => {
                Toast.makeText(localize("ErrorLogin"), 'long').show();
                console.log("error login:" + JSON.stringify(err));
                this.error = localize("ErrorLogin");
                this.inProgress = false;
            });
        }
    }

    setShowRegistration(reg) {
        this.register = reg;
        this.cdr.detectChanges();
    }
}
