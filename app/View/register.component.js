"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_angular_1 = require("nativescript-angular");
var user_service_1 = require("../Service/user.service");
var nativescript_localize_1 = require("nativescript-localize");
var Toast = require("nativescript-toast");
var page_1 = require("tns-core-modules/ui/page");
var RegisterComponent = (function () {
    function RegisterComponent(routerExtensions, userService, page, cdr) {
        var _this = this;
        this.routerExtensions = routerExtensions;
        this.userService = userService;
        this.page = page;
        this.cdr = cdr;
        this.error = "";
        this.username = "";
        this.email = "";
        this.password = "";
        this.passwordRepeat = "";
        this.inProgress = false;
        this.page.on(page_1.Page.navigatedToEvent, function () {
            _this.inProgress = false;
            _this.register = false;
            _this.cdr.detectChanges();
        });
    }
    RegisterComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };
    RegisterComponent.prototype.submit = function () {
        var _this = this;
        if (this.inProgress)
            return;
        this.password = this.passwordRef.nativeElement.text;
        this.email = this.emailRef.nativeElement.text;
        this.error = "";
        var invalid = false;
        if (!this.validateEmail(this.email)) {
            invalid = true;
            this.error += nativescript_localize_1.localize("InvalidEmail") + "\n";
        }
        if (!this.password || this.password.length < 6) {
            invalid = true;
            this.error += nativescript_localize_1.localize("InvalidPassword") + "\n";
        }
        if (this.register) {
            this.passwordRepeat = this.passwordRepeatRef.nativeElement.text;
            this.username = this.usernameRef.nativeElement.text;
            if (this.password != this.passwordRepeat) {
                invalid = true;
                this.error += nativescript_localize_1.localize("InvalidPassword") + "\n";
            }
            if (!this.username) {
                invalid = true;
                this.error += nativescript_localize_1.localize("InvalidUsername") + "\n";
            }
            if (invalid)
                return;
            this.inProgress = true;
            this.userService.register(this.username, this.email, this.passwordRepeat).then(function (u) {
                Toast.makeText(nativescript_localize_1.localize("RegistrationWasSuccessful"), 'long').show();
                _this.routerExtensions.back();
                _this.inProgress = false;
            }).catch(function (err) {
                console.log("Error register:" + JSON.stringify(err));
                Toast.makeText(nativescript_localize_1.localize("ErrorRegister"), 'long').show();
                _this.error += nativescript_localize_1.localize("ErrorRegister");
                _this.inProgress = false;
            });
        }
        else {
            // LOGIN
            if (invalid)
                return;
            this.inProgress = true;
            this.userService.tryLogin(this.email, this.password).then(function (u) {
                Toast.makeText(nativescript_localize_1.localize("LoginWasSuccessful"), 'long').show();
                _this.routerExtensions.back();
                _this.inProgress = false;
            }).catch(function (err) {
                Toast.makeText(nativescript_localize_1.localize("ErrorLogin"), 'long').show();
                console.log("error login:" + JSON.stringify(err));
                _this.error = nativescript_localize_1.localize("ErrorLogin");
                _this.inProgress = false;
            });
        }
    };
    RegisterComponent.prototype.setShowRegistration = function (reg) {
        this.register = reg;
        this.cdr.detectChanges();
    };
    __decorate([
        core_1.ViewChild('email'),
        __metadata("design:type", core_1.ElementRef)
    ], RegisterComponent.prototype, "emailRef", void 0);
    __decorate([
        core_1.ViewChild('username'),
        __metadata("design:type", core_1.ElementRef)
    ], RegisterComponent.prototype, "usernameRef", void 0);
    __decorate([
        core_1.ViewChild('password'),
        __metadata("design:type", core_1.ElementRef)
    ], RegisterComponent.prototype, "passwordRef", void 0);
    __decorate([
        core_1.ViewChild('passwordRepeat'),
        __metadata("design:type", core_1.ElementRef)
    ], RegisterComponent.prototype, "passwordRepeatRef", void 0);
    RegisterComponent = __decorate([
        core_1.Component({
            selector: "Register",
            moduleId: module.id,
            templateUrl: "./register.html",
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions,
            user_service_1.UserService,
            page_1.Page, core_1.ChangeDetectorRef])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
