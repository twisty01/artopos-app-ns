import {Component, Input} from "@angular/core";

@Component({
    selector: 'PostItem',
    moduleId: module.id,
    template: `
<GridLayout columns="auto,*,auto" rows="*" class="post-item {{ post.resolved && post.resolved != 'false' ? 'post-resolved' : ''}}">
    <FlexboxLayout class="post-item__text post-item" col="1"  padding="0">
        <FlexboxLayout>
            <Image [visibility]="post.lat && post.lng ? 'visible': 'collapse'" [src]="post.resolved && post.resolved != 'false' ? '~/assets/img/marker-green.png': '~/assets/img/marker-red.png'" height="30" stretch="aspectFit"></Image>
            <Label class="post-item__name" [text]="post.userName + (post.created ? ',  ' + post.created : '')"></Label>
        </FlexboxLayout>
        <Label class="post-item__desc" [text]="post.text || ('WasHere'|L)" textWrap="true" ></Label>
    </FlexboxLayout >
    <StackLayout class="post-item__img-container" col="{{ odd ? 0 : 2}}" width="120" horizontalAlignment="center">
        <Image stretch="aspectFit" width="120"  [src]="post.tn || 'res://placeholder'"></Image>
    </StackLayout>
</GridLayout>`
})
export class PostItemComponent {
    @Input() post: any = {};
    @Input() odd: boolean;
    constructor() { }
}
