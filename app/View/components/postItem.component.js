"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PostItemComponent = (function () {
    function PostItemComponent() {
        this.post = {};
    }
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PostItemComponent.prototype, "post", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], PostItemComponent.prototype, "odd", void 0);
    PostItemComponent = __decorate([
        core_1.Component({
            selector: 'PostItem',
            moduleId: module.id,
            template: "\n<GridLayout columns=\"auto,*,auto\" rows=\"*\" class=\"post-item {{ post.resolved && post.resolved != 'false' ? 'post-resolved' : ''}}\">\n    <FlexboxLayout class=\"post-item__text post-item\" col=\"1\"  padding=\"0\">\n        <FlexboxLayout>\n            <Image [visibility]=\"post.lat && post.lng ? 'visible': 'collapse'\" [src]=\"post.resolved && post.resolved != 'false' ? '~/assets/img/marker-green.png': '~/assets/img/marker-red.png'\" height=\"30\" stretch=\"aspectFit\"></Image>\n            <Label class=\"post-item__name\" [text]=\"post.userName + (post.created ? ',  ' + post.created : '')\"></Label>\n        </FlexboxLayout>\n        <Label class=\"post-item__desc\" [text]=\"post.text || ('WasHere'|L)\" textWrap=\"true\" ></Label>\n    </FlexboxLayout >\n    <StackLayout class=\"post-item__img-container\" col=\"{{ odd ? 0 : 2}}\" width=\"120\" horizontalAlignment=\"center\">\n        <Image stretch=\"aspectFit\" width=\"120\"  [src]=\"post.tn || 'res://placeholder'\"></Image>\n    </StackLayout>\n</GridLayout>"
        }),
        __metadata("design:paramtypes", [])
    ], PostItemComponent);
    return PostItemComponent;
}());
exports.PostItemComponent = PostItemComponent;
