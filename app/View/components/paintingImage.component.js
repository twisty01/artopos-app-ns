"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_angular_1 = require("nativescript-angular");
var painting_service_1 = require("../../Service/painting.service");
var post_service_1 = require("../../Service/post.service");
var prefs = require("tns-core-modules/application-settings");
var painting_component_1 = require("../painting.component");
require("rxjs/add/operator/switchMap");
var PaintingImageComponent = (function () {
    function PaintingImageComponent(routerExtensions, paintingService, postService, pageRoute) {
        this.routerExtensions = routerExtensions;
        this.paintingService = paintingService;
        this.postService = postService;
        this.pageRoute = pageRoute;
        this.items = [];
        this.painting = {};
    }
    PaintingImageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pageRoute.activatedRoute.switchMap(function (r) { return r.params; }).forEach(function (p) { return _this.loadData(+p["id"]); });
    };
    PaintingImageComponent.prototype.loadData = function (id) {
        var _this = this;
        return this.paintingService.getPainting(id).then(function (painting) {
            _this.painting = painting;
            _this.items = [];
            if (!painting)
                _this.routerExtensions.back();
            _this.items.push({ imageUrl: painting.img });
            var postImgsStr = prefs.getString(painting_component_1.PAINTING_POST_IMGS);
            if (postImgsStr) {
                _this.items.push({ imageUrl: postImgsStr });
                _this.pageNumber = 1;
            }
        });
    };
    PaintingImageComponent = __decorate([
        core_1.Component({
            selector: "PaintingImageComponent",
            moduleId: module.id,
            template: "\n        <ActionBar class=\"action-bar\">\n            <StatusBarStyle></StatusBarStyle>\n            <NavigationButton class=\"nav-btn\" text=\"back\" icon=\"res://ic_chevron_left\"\n                              (tap)=\"routerExtensions.back()\"></NavigationButton>\n            <StackLayout verticalAlign=\"center\" horizontalAlign=\"center\" alignItems=\"center\" justifyContent=\"center\"\n                         width=\"100%\">\n                <Label class=\"painting-title-name\" textWrap=\"true\" text=\"{{painting.name}}\"></Label>\n                <Label class=\"painting-title-author\" text=\"{{painting.authorName}}\"></Label>\n            </StackLayout>\n        </ActionBar>\n        \n        <GridLayout>\n            <ImageSwipe [items]=\"items\" imageUrlProperty=\"imageUrl\" class=\"image-view\" [pageNumber]=\"pageNumber\">\n            </ImageSwipe>\n        </GridLayout>",
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions, painting_service_1.PaintingService,
            post_service_1.PostService, nativescript_angular_1.PageRoute])
    ], PaintingImageComponent);
    return PaintingImageComponent;
}());
exports.PaintingImageComponent = PaintingImageComponent;
