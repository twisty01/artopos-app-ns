import {Component, ViewChild, ElementRef, Input} from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { GeocodingService } from "../../Service/geocoding.service";
import { debounce } from "../../Helpers/debounce";
import { AnimationCurve } from "tns-core-modules/ui/enums/enums";
import { Bounds, Position } from "nativescript-google-maps-sdk";
import { isAndroid } from "tns-core-modules/platform/platform";
import * as app from "tns-core-modules/application";

declare var android: any; //bypass the TS warnings

@Component({
    selector: "MapSearch",
    moduleId: module.id,
    templateUrl: './mapSearch.html'
})
export class MapSearchComponent {

    @Input()
    mapView: any; 

    private searchOn: boolean = false;
    private searchItems = [];

    @ViewChild('searchWidget')
    private searchWidget: ElementRef;
    @ViewChild('searchInput')
    private searchInput: ElementRef;

    private searchActionBarBtn;

    constructor(private page: Page, private geocoding: GeocodingService) {}

    ngOnInit(): void {
        this.page.on(Page.navigatedToEvent, () => {
            if (isAndroid) {
                let searchInput = this.searchInput.nativeElement;
                searchInput.dismissSoftInput();
                app.android.startActivity.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        });
        this.searchActionBarBtn = this.page.actionBar.getViewById("homeMapToggleSearchButton");
        this.searchActionBarBtn.on("tap", this.toggleSearch);
    }

    toggleSearch = (e = null) => {
        this.searchOn = !this.searchOn;
        this.searchActionBarBtn.icon = this.searchOn ? "res://close" : "res://search";
        if (this.searchOn) {
            this.searchWidget.nativeElement.animate({
                translate: {x: 0, y: 0},
                duration: 100,
                curve: AnimationCurve.easeInOut
            });
        } else {
            this.searchInput.nativeElement.dismissSoftInput();
            this.searchWidget.nativeElement.animate({
                translate: {x: 0, y: -60},
                duration: 100,
                curve: AnimationCurve.easeInOut
            });
        }
    };

    search = debounce((args = null) => {
        let txt = this.searchInput.nativeElement.text;
        if (txt.length < 3) return;
        this.geocoding.getPlaces(txt).then((results: any) => {
           this.searchItems = results;
        });
    }, 700);

    resultClick(placeItem) {
        if (!placeItem) return;
        this.geocoding.getPlace(placeItem).then(place => {
            const bounds = place.data.result.geometry.viewport;
            this.mapView.setViewport(Bounds.fromCoordinates(
                Position.positionFromLatLng(bounds.southwest.lat,bounds.southwest.lng),
                Position.positionFromLatLng(bounds.northeast.lat,bounds.northeast.lng),
            ));
        });
        this.toggleSearch();
    }

    onItemTap($event){
        this.resultClick(this.searchItems[$event.index]);
    }

    clearSearch() {
        this.searchItems = [];
        this.searchInput.nativeElement.text = "";
    }
}