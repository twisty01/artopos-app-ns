import {OnInit, Component} from "@angular/core";
import {PageRoute, RouterExtensions} from "nativescript-angular";
import {PaintingService} from "../../Service/painting.service";
import {PostService} from "../../Service/post.service";
import {Painting} from "../../Model/painting";
import {Post} from "../../Model/post";
import * as prefs from "tns-core-modules/application-settings";
import {PAINTING_POST_IMGS} from "../painting.component";
import "rxjs/add/operator/switchMap";

@Component({
    selector: "PaintingImageComponent",
    moduleId: module.id,
    template: `
        <ActionBar class="action-bar">
            <StatusBarStyle></StatusBarStyle>
            <NavigationButton class="nav-btn" text="back" icon="res://ic_chevron_left"
                              (tap)="routerExtensions.back()"></NavigationButton>
            <StackLayout verticalAlign="center" horizontalAlign="center" alignItems="center" justifyContent="center"
                         width="100%">
                <Label class="painting-title-name" textWrap="true" text="{{painting.name}}"></Label>
                <Label class="painting-title-author" text="{{painting.authorName}}"></Label>
            </StackLayout>
        </ActionBar>
        
        <GridLayout>
            <ImageSwipe [items]="items" imageUrlProperty="imageUrl" class="image-view" [pageNumber]="pageNumber">
            </ImageSwipe>
        </GridLayout>`,
})
export class PaintingImageComponent implements OnInit {
    public items: any[] = [];
    public pageNumber: number;

    public painting = {};

    constructor(private routerExtensions: RouterExtensions, private paintingService: PaintingService,
                private postService: PostService, private pageRoute: PageRoute) {}

    ngOnInit(): void {
        this.pageRoute.activatedRoute.switchMap(r => r.params).forEach(
            p => this.loadData(+p["id"])
        )
    }

    private loadData(id) {
        return this.paintingService.getPainting(id).then((painting) => {
            this.painting = painting;
            this.items = [];
            if (!painting) this.routerExtensions.back();
            this.items.push({imageUrl: painting.img});
            const postImgsStr = prefs.getString(PAINTING_POST_IMGS);
            if (postImgsStr) {
                this.items.push({imageUrl: postImgsStr});
                this.pageNumber = 1;
            }
        })
    }
}