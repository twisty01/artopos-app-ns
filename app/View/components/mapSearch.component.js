"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var page_1 = require("tns-core-modules/ui/page/page");
var geocoding_service_1 = require("../../Service/geocoding.service");
var debounce_1 = require("../../Helpers/debounce");
var enums_1 = require("tns-core-modules/ui/enums/enums");
var nativescript_google_maps_sdk_1 = require("nativescript-google-maps-sdk");
var platform_1 = require("tns-core-modules/platform/platform");
var app = require("tns-core-modules/application");
var MapSearchComponent = (function () {
    function MapSearchComponent(page, geocoding) {
        var _this = this;
        this.page = page;
        this.geocoding = geocoding;
        this.searchOn = false;
        this.searchItems = [];
        this.toggleSearch = function (e) {
            if (e === void 0) { e = null; }
            _this.searchOn = !_this.searchOn;
            _this.searchActionBarBtn.icon = _this.searchOn ? "res://close" : "res://search";
            if (_this.searchOn) {
                _this.searchWidget.nativeElement.animate({
                    translate: { x: 0, y: 0 },
                    duration: 100,
                    curve: enums_1.AnimationCurve.easeInOut
                });
            }
            else {
                _this.searchInput.nativeElement.dismissSoftInput();
                _this.searchWidget.nativeElement.animate({
                    translate: { x: 0, y: -60 },
                    duration: 100,
                    curve: enums_1.AnimationCurve.easeInOut
                });
            }
        };
        this.search = debounce_1.debounce(function (args) {
            if (args === void 0) { args = null; }
            var txt = _this.searchInput.nativeElement.text;
            if (txt.length < 3)
                return;
            _this.geocoding.getPlaces(txt).then(function (results) {
                _this.searchItems = results;
            });
        }, 700);
    }
    MapSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.on(page_1.Page.navigatedToEvent, function () {
            if (platform_1.isAndroid) {
                var searchInput = _this.searchInput.nativeElement;
                searchInput.dismissSoftInput();
                app.android.startActivity.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        });
        this.searchActionBarBtn = this.page.actionBar.getViewById("homeMapToggleSearchButton");
        this.searchActionBarBtn.on("tap", this.toggleSearch);
    };
    MapSearchComponent.prototype.resultClick = function (placeItem) {
        var _this = this;
        if (!placeItem)
            return;
        this.geocoding.getPlace(placeItem).then(function (place) {
            var bounds = place.data.result.geometry.viewport;
            _this.mapView.setViewport(nativescript_google_maps_sdk_1.Bounds.fromCoordinates(nativescript_google_maps_sdk_1.Position.positionFromLatLng(bounds.southwest.lat, bounds.southwest.lng), nativescript_google_maps_sdk_1.Position.positionFromLatLng(bounds.northeast.lat, bounds.northeast.lng)));
        });
        this.toggleSearch();
    };
    MapSearchComponent.prototype.onItemTap = function ($event) {
        this.resultClick(this.searchItems[$event.index]);
    };
    MapSearchComponent.prototype.clearSearch = function () {
        this.searchItems = [];
        this.searchInput.nativeElement.text = "";
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], MapSearchComponent.prototype, "mapView", void 0);
    __decorate([
        core_1.ViewChild('searchWidget'),
        __metadata("design:type", core_1.ElementRef)
    ], MapSearchComponent.prototype, "searchWidget", void 0);
    __decorate([
        core_1.ViewChild('searchInput'),
        __metadata("design:type", core_1.ElementRef)
    ], MapSearchComponent.prototype, "searchInput", void 0);
    MapSearchComponent = __decorate([
        core_1.Component({
            selector: "MapSearch",
            moduleId: module.id,
            templateUrl: './mapSearch.html'
        }),
        __metadata("design:paramtypes", [page_1.Page, geocoding_service_1.GeocodingService])
    ], MapSearchComponent);
    return MapSearchComponent;
}());
exports.MapSearchComponent = MapSearchComponent;
