"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var frame_1 = require("ui/frame");
var StatusbarstyleComponent = (function () {
    function StatusbarstyleComponent() {
    }
    StatusbarstyleComponent_1 = StatusbarstyleComponent;
    StatusbarstyleComponent.prototype.ngOnInit = function () {
        StatusbarstyleComponent_1.setActionBarStyle();
    };
    StatusbarstyleComponent.setActionBarStyle = function () {
        if (frame_1.topmost().ios) {
            var navigationBar = frame_1.topmost().ios.controller.navigationBar;
            navigationBar.translucent = false;
            navigationBar.setBackgroundImageForBarMetrics(UIImage.new(), UIBarMetrics.Default);
            navigationBar.shadowImage = UIImage.new();
            navigationBar.barStyle = UIBarStyle.UIBarStyleBlack;
        }
    };
    StatusbarstyleComponent = StatusbarstyleComponent_1 = __decorate([
        core_1.Component({
            selector: "StatusBarStyle",
            moduleId: module.id,
            template: "<StatusBar android:barStyle=\"#2c2b30\"></StatusBar>",
        })
    ], StatusbarstyleComponent);
    return StatusbarstyleComponent;
    var StatusbarstyleComponent_1;
}());
exports.StatusbarstyleComponent = StatusbarstyleComponent;
