import {Component, OnInit} from "@angular/core";
import {topmost} from "ui/frame";


declare let UIImage: any;
declare let UIBarMetrics: any;
declare let UIBarStyle: any;

@Component({
    selector: "StatusBarStyle",
    moduleId: module.id,
    template: `<StatusBar android:barStyle="#2c2b30"></StatusBar>`,
})
export class StatusbarstyleComponent implements OnInit {
    ngOnInit(): void {
        StatusbarstyleComponent.setActionBarStyle();
    }

    public static setActionBarStyle() {
        if (topmost().ios) {
            const navigationBar = topmost().ios.controller.navigationBar;
            navigationBar.translucent = false;
            navigationBar.setBackgroundImageForBarMetrics(UIImage.new(), UIBarMetrics.Default);
            navigationBar.shadowImage = UIImage.new();
            navigationBar.barStyle = UIBarStyle.UIBarStyleBlack;
        }
    }

}