import {Component, Input} from "@angular/core";

@Component({
    selector: 'PaintingItem',
    moduleId: module.id,
    template: `
        <!--<GridLayout columns="*" rows="*"-->
                    <!--class="painting-item {{ painting.resolved && painting.resolved != 'false' ? 'painting-resolved' : ''}}"-->
                    <!--padding="0">-->
            <!--<Image stretch="aspectFit" [src]="painting.tn" row="0"></Image>-->
            <!--<StackLayout class="painting-item__text" row="0" width="100%">-->
                <!--<FlexboxLayout class="painting-item__text-bg" justifyContent="space-between">-->
                    <!--<StackLayout orientation="horizontal">-->
                        <!--<Label class="painting-item__name" [text]="painting.name"></Label>-->
                        <!--<Label class="painting-item__info" [text]="',  ' + painting.authorName"></Label>-->
                    <!--</StackLayout>-->
                    <!--<Image [src]="painting.resolved && painting.resolved != 'false' ? '~/assets/img/marker-green.png': '~/assets/img/marker-red.png'"-->
                           <!--height="20" stretch="aspectFit" marginLeft="5"></Image>-->
                <!--</FlexboxLayout>-->
            <!--</StackLayout>-->
        <!--</GridLayout>-->
<FlexboxLayout class="selected-painting painting-item" justifyContent="center" backgroundColor="white">
    <StackLayout class="selected-painting__center">
        <Image [src]="painting.tn ? painting.tn : ''" ></Image>
        <StackLayout class="selected-painting__label" width="80%">
            <Label class="selected-painting__name" [text]="painting.name" color="black"></Label>
            <Label class="selected-painting__info" color="black"
                   text="{{ painting.authorName?painting.authorName:('UnknownAuthor'|L)}}{{painting.paintingCreated?', '+painting.paintingCreated:''}}"></Label>
            <Label [text]="(painting.resolved&&painting.resolved!='false'?'ResolvedLabel':'NotResolvedLabel')|L"
                   [class]="painting.resolved&&painting.resolved!='false'?'resolved-label':'not-resolved-label'"></Label>
        </StackLayout>
    </StackLayout>
</FlexboxLayout>`
})
export class PaintingItemComponent {
    @Input() painting: any = {};
    @Input() odd: boolean;

    constructor() { }
}

