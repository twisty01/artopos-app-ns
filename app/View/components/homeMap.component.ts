import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {MapView, Marker, Position, MarkerEventData} from "nativescript-google-maps-sdk";
import {screen} from "platform"
import * as geolocation from "nativescript-geolocation";
import {PaintingService} from "../../Service/painting.service";
import * as connectivity from "tns-core-modules/connectivity";
import {RouterExtensions} from "nativescript-angular";
import {debounce} from "../../Helpers/debounce";
import {Page} from "tns-core-modules/ui/page";

declare var android: any; //bypass the TS warnings

@Component({
    selector: "HomeMap",
    moduleId: module.id,
    templateUrl: './homeMap.html'
})
export class HomeMapComponent implements OnInit {
    lat = 50.069911;
    lng = 14.472972;
    zoom = 10;
    private bounds = {
        southwest:{
            latitude:49.27694065808714,
            longitude:13.225570991635323,
        },
        northeast:{
            latitude:51.00281477522822,
            longitude:15.443763546645641
        }
    };

    private mapView: MapView;
    private lastCamera: String;

    selectedMarker = null;
    selectedPainting;

    @ViewChild('mapContainer')
    mapContainer: ElementRef;

    markers = [];

    loading;

    constructor(private page: Page, private paintingService: PaintingService, private routerExtensions: RouterExtensions) {}

    ngOnInit(): void {
        this.page.on(Page.navigatedToEvent, ()=>{
            this.mapContainer.nativeElement.animate({
                translate: {x: 0, y: 0},
                duration: 200,
            });
        });
    }

    onMapReady(event) {
        this.mapView = event.object;
        this.mapView.setStyle(require("~/assets/map_styles_light.json"));

        this.mapView.settings.mapToolbarEnabled = true;
        this.mapView.settings.rotateGesturesEnabled = false;
        this.mapView.settings.tiltGesturesEnabled = false;
        this.mapView.settings.mapToolbarEnabled = true;
        this.mapView.settings.zoomControlsEnabled = true;
        this.mapView.minZoom = 7;

        geolocation.enableLocationRequest(true).then(() => {
            this.mapView.myLocationEnabled = true;
            this.mapView.settings.myLocationButtonEnabled = true;
        }).catch(er => console.log("LOCATION REJECTED : " + er));
        this.bounds = this.mapView.projection.visibleRegion.bounds;
        this.loadData();
    }

    loadData(){
        this.loading = true;
        this.paintingService.loadPaintings().then(() => {
            this.loading = false;
            this.addMarkers();
        })
    };

    addMarkers() {
        if (!this.paintingService.paintingsLoaded) return;
        this.markers.forEach((m)=>{if (m && m != this.selectedMarker) this.mapView.removeMarker(m)});
        this.markers = [this.selectedMarker];
        this.paintingService.getPaintingsByBounds(this.bounds).then((res) => {
            for (let painting of res) {
                if(!this.selectedMarker||!this.selectedMarker.userData||this.selectedMarker.userData.id!=painting.id){
                    let marker = new Marker();
                    marker.position = Position.positionFromLatLng(painting.lat, painting.lng);
                    marker.userData = painting;
                    marker.color = (painting.resolved as any)== "true" ? "green" : "red";
                    this.mapView.addMarker(marker);
                    this.markers.push(marker);
                }
            }
        });
    }

    onMarkerEvent(args: MarkerEventData) {
        this.selectMarker(args.marker);
    }

    selectMarker(marker) {
        this.selectedMarker = marker;
        this.selectedPainting = marker.userData;
    }

    deselect(){
        this.selectedMarker = null;
        this.selectedPainting = null;
    }

    onCameraChanged = debounce((args) => {
        const newCam = JSON.stringify(args.camera);
        if (newCam === this.lastCamera) return;
        this.lastCamera = newCam;
        this.bounds = this.mapView.projection.visibleRegion.bounds;
        this.addMarkers();
    }, 1000);

    goToDetail() {
        if (!this.selectedMarker || !this.selectedMarker.userData) return;
        this.mapContainer.nativeElement.animate({
            translate: {x: 0, y: -screen.mainScreen.heightDIPs + 40},
            duration: 200,
        }).then(() => {
            this.routerExtensions.navigate(["/painting", this.selectedMarker.userData.id], {
                // transition: {
                //     name: "fade",
                //     duration: 100,
                //     curve: AnimationCurve.easeInOut
                // }
            })
        })
    }
}