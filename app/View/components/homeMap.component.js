"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_google_maps_sdk_1 = require("nativescript-google-maps-sdk");
var platform_1 = require("platform");
var geolocation = require("nativescript-geolocation");
var painting_service_1 = require("../../Service/painting.service");
var nativescript_angular_1 = require("nativescript-angular");
var debounce_1 = require("../../Helpers/debounce");
var page_1 = require("tns-core-modules/ui/page");
var HomeMapComponent = (function () {
    function HomeMapComponent(page, paintingService, routerExtensions) {
        var _this = this;
        this.page = page;
        this.paintingService = paintingService;
        this.routerExtensions = routerExtensions;
        this.lat = 50.069911;
        this.lng = 14.472972;
        this.zoom = 10;
        this.bounds = {
            southwest: {
                latitude: 49.27694065808714,
                longitude: 13.225570991635323,
            },
            northeast: {
                latitude: 51.00281477522822,
                longitude: 15.443763546645641
            }
        };
        this.selectedMarker = null;
        this.markers = [];
        this.onCameraChanged = debounce_1.debounce(function (args) {
            var newCam = JSON.stringify(args.camera);
            if (newCam === _this.lastCamera)
                return;
            _this.lastCamera = newCam;
            _this.bounds = _this.mapView.projection.visibleRegion.bounds;
            _this.addMarkers();
        }, 1000);
    }
    HomeMapComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.on(page_1.Page.navigatedToEvent, function () {
            _this.mapContainer.nativeElement.animate({
                translate: { x: 0, y: 0 },
                duration: 200,
            });
        });
    };
    HomeMapComponent.prototype.onMapReady = function (event) {
        var _this = this;
        this.mapView = event.object;
        this.mapView.setStyle(require("~/assets/map_styles_light.json"));
        this.mapView.settings.mapToolbarEnabled = true;
        this.mapView.settings.rotateGesturesEnabled = false;
        this.mapView.settings.tiltGesturesEnabled = false;
        this.mapView.settings.mapToolbarEnabled = true;
        this.mapView.settings.zoomControlsEnabled = true;
        this.mapView.minZoom = 7;
        geolocation.enableLocationRequest(true).then(function () {
            _this.mapView.myLocationEnabled = true;
            _this.mapView.settings.myLocationButtonEnabled = true;
        }).catch(function (er) { return console.log("LOCATION REJECTED : " + er); });
        this.bounds = this.mapView.projection.visibleRegion.bounds;
        this.loadData();
    };
    HomeMapComponent.prototype.loadData = function () {
        var _this = this;
        this.loading = true;
        this.paintingService.loadPaintings().then(function () {
            _this.loading = false;
            _this.addMarkers();
        });
    };
    ;
    HomeMapComponent.prototype.addMarkers = function () {
        var _this = this;
        if (!this.paintingService.paintingsLoaded)
            return;
        this.markers.forEach(function (m) { if (m && m != _this.selectedMarker)
            _this.mapView.removeMarker(m); });
        this.markers = [this.selectedMarker];
        this.paintingService.getPaintingsByBounds(this.bounds).then(function (res) {
            for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                var painting = res_1[_i];
                if (!_this.selectedMarker || !_this.selectedMarker.userData || _this.selectedMarker.userData.id != painting.id) {
                    var marker = new nativescript_google_maps_sdk_1.Marker();
                    marker.position = nativescript_google_maps_sdk_1.Position.positionFromLatLng(painting.lat, painting.lng);
                    marker.userData = painting;
                    marker.color = painting.resolved == "true" ? "green" : "red";
                    _this.mapView.addMarker(marker);
                    _this.markers.push(marker);
                }
            }
        });
    };
    HomeMapComponent.prototype.onMarkerEvent = function (args) {
        this.selectMarker(args.marker);
    };
    HomeMapComponent.prototype.selectMarker = function (marker) {
        this.selectedMarker = marker;
        this.selectedPainting = marker.userData;
    };
    HomeMapComponent.prototype.deselect = function () {
        this.selectedMarker = null;
        this.selectedPainting = null;
    };
    HomeMapComponent.prototype.goToDetail = function () {
        var _this = this;
        if (!this.selectedMarker || !this.selectedMarker.userData)
            return;
        this.mapContainer.nativeElement.animate({
            translate: { x: 0, y: -platform_1.screen.mainScreen.heightDIPs + 40 },
            duration: 200,
        }).then(function () {
            _this.routerExtensions.navigate(["/painting", _this.selectedMarker.userData.id], {});
        });
    };
    __decorate([
        core_1.ViewChild('mapContainer'),
        __metadata("design:type", core_1.ElementRef)
    ], HomeMapComponent.prototype, "mapContainer", void 0);
    HomeMapComponent = __decorate([
        core_1.Component({
            selector: "HomeMap",
            moduleId: module.id,
            templateUrl: './homeMap.html'
        }),
        __metadata("design:paramtypes", [page_1.Page, painting_service_1.PaintingService, nativescript_angular_1.RouterExtensions])
    ], HomeMapComponent);
    return HomeMapComponent;
}());
exports.HomeMapComponent = HomeMapComponent;
