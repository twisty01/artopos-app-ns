"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("../../Service/user.service");
var nativescript_angular_1 = require("nativescript-angular");
var page_1 = require("tns-core-modules/ui/page/page");
var post_service_1 = require("../../Service/post.service");
var dialogs = require("ui/dialogs");
var nativescript_localize_1 = require("nativescript-localize");
var utils = require("tns-core-modules/utils/utils");
var HomeProfileComponent = (function () {
    function HomeProfileComponent(routerExtensions, userService, page, postService) {
        var _this = this;
        this.routerExtensions = routerExtensions;
        this.userService = userService;
        this.page = page;
        this.postService = postService;
        this.loggedIn = false;
        this.user = {};
        this.posts = [];
        this.loading = false;
        this.logout = function () {
            _this.userService.logout();
            _this.user = {};
            _this.posts = [];
            _this.loggedIn = false;
            if (_this.home) {
                _this.home.changeIndex(_this.home.oldIndex);
            }
        };
    }
    HomeProfileComponent.prototype.ngOnInit = function () {
        this.page.actionBar.getViewById("homeProfileLogoutButton").on("tap", this.logout);
    };
    HomeProfileComponent.prototype.tabSelected = function (home) {
        var _this = this;
        this.home = home;
        if (this.loggedIn)
            return;
        if (this.userService.isLogged()) {
            this.loading = true;
            this.userService.getCurrentUser().then(function (u) {
                _this.loggedIn = true;
                _this.user = u;
                _this.postService.setPostsByUser(u.id).then(function (res) {
                    _this.posts = res;
                    _this.loading = false;
                }).catch(function () { return _this.loading = false; });
            }).catch(function () { return _this.loading = false; });
        }
        else {
            this.loggedIn = false;
            dialogs.confirm({
                title: nativescript_localize_1.localize("Login"),
                message: nativescript_localize_1.localize("ProfileLoginMessage"),
                okButtonText: nativescript_localize_1.localize("Login") + "/" + nativescript_localize_1.localize("Register"),
                neutralButtonText: nativescript_localize_1.localize("Cancel")
            }).then(function (result) {
                if (result) {
                    _this.routerExtensions.navigate(['/register']);
                }
                else {
                    home.changeIndex(home.oldIndex);
                }
            }).catch(function () { return home.changeIndex(home.oldIndex); });
        }
    };
    HomeProfileComponent.prototype.tabDeselected = function () {
        this.home = null;
    };
    HomeProfileComponent.prototype.postItemTap = function (args) {
        console.log("PostItem Tapped at cell index: " + args.index);
        this.routerExtensions.navigate(["/painting", this.posts[args.index].paintingId]);
    };
    HomeProfileComponent.prototype.onListViewLoaded = function (event) {
        var lv = event.object;
        if (lv.android) {
            var id = utils.ad.resources.getId('home_profile_list_view');
            lv.android.setId(id);
        }
    };
    HomeProfileComponent = __decorate([
        core_1.Component({
            selector: "HomeProfile",
            moduleId: module.id,
            templateUrl: "./homeProfile.html",
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions, user_service_1.UserService, page_1.Page, post_service_1.PostService])
    ], HomeProfileComponent);
    return HomeProfileComponent;
}());
exports.HomeProfileComponent = HomeProfileComponent;
