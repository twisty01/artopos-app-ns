import {AfterViewInit, Component, OnInit, Output} from "@angular/core";
import {UserService} from "../../Service/user.service";
import {RouterExtensions} from "nativescript-angular";
import {Page} from "tns-core-modules/ui/page/page";
import {TabSelected} from "../../Helpers/tabSelected";
import {HomeComponent} from "../home.component";
import {PostService} from "../../Service/post.service";
import {User} from "../../Model/user";
import * as dialogs from "ui/dialogs";
import {localize} from "nativescript-localize";
import * as utils from "tns-core-modules/utils/utils";

@Component({
    selector: "HomeProfile",
    moduleId: module.id,
    templateUrl: "./homeProfile.html",
})
export class HomeProfileComponent implements TabSelected, OnInit {


    loggedIn = false;
    user: User | any = {};
    posts = [];

    loading = false;

    private home;

    constructor(private routerExtensions: RouterExtensions, private userService: UserService, private page: Page, private postService: PostService) {}

    ngOnInit(): void {
        this.page.actionBar.getViewById("homeProfileLogoutButton").on("tap", this.logout);
    }

    tabSelected(home: HomeComponent): void {
        this.home = home;
        if (this.loggedIn) return;

        if (this.userService.isLogged()) {
            this.loading = true;
            this.userService.getCurrentUser().then(u => {
                this.loggedIn = true;
                this.user = u;
                this.postService.setPostsByUser(u.id).then((res) => {
                    this.posts = res;
                    this.loading = false;
                }).catch(() => this.loading = false);
            }).catch(() => this.loading = false);
        } else {
            this.loggedIn = false;
            dialogs.confirm({
                title: localize("Login"),
                message: localize("ProfileLoginMessage"),
                okButtonText: localize("Login") + "/" + localize("Register"),
                neutralButtonText: localize("Cancel")
            }).then((result) => {
                if (result) {
                    this.routerExtensions.navigate(['/register']);
                } else {
                    home.changeIndex(home.oldIndex);
                }
            }).catch(() => home.changeIndex(home.oldIndex));
        }
    }

    tabDeselected() {
        this.home = null;
    }

    logout = () => {
        this.userService.logout();
        this.user = {};
        this.posts = [];
        this.loggedIn = false;
        if (this.home) {
            this.home.changeIndex(this.home.oldIndex);
        }
    };

    postItemTap(args) {
        console.log("PostItem Tapped at cell index: " + args.index);
        this.routerExtensions.navigate(["/painting", this.posts[args.index].paintingId],);
    }

    onListViewLoaded(event){
        const lv = event.object;
        if (lv.android) {
            const id = utils.ad.resources.getId('home_profile_list_view');
            lv.android.setId(id);
        }
    }

}