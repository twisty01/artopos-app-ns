import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {TabSelected} from "../../Helpers/tabSelected";
import {HomeComponent} from "../home.component";
import {RouterExtensions} from "nativescript-angular";
import {PaintingService} from "../../Service/painting.service";
import {Page} from "tns-core-modules/ui/page";
import * as prefs from "application-settings";
import {SearchBar} from "tns-core-modules/ui/search-bar";
import {isAndroid} from "tns-core-modules/platform";
import * as app from "tns-core-modules/application";
import * as Toast from "nativescript-toast";
import * as utils from 'utils/utils';

declare var android: any; //bypass the TS warnings

export const FILTER_OPTIONS_KEY = "artopos.filter_options";

export class FilterOptions {
    bounds;
    resolved;
    institutionName;
    address;
}

@Component({
    selector: "HomeList",
    moduleId: module.id,
    template: `
        <GridLayout backgroundColor="white" rows="auto,*">
            <SearchBar hint="Zadejte název díla nebo jméno autora" #listSearchInput
                       [text]="searchPhrase"
                       (submit)="onSubmit($event)"
                       (clear)="onClear($event)"
                       textFieldHintColor="#cdcdcd"
                       color="#ffffff"
                       row="0"
                       backgroundColor="#2c2b30"></SearchBar>

            <ActivityIndicator row="1" [busy]="inProgress" class="activity-indicator"></ActivityIndicator>

            <Label row="1" [visibility]="!inProgress && !paintings.length ? 'visible':'collapse'"
                   [text]="'NoPaintings'|L" textAlignment="center"></Label>

            <ListView row="1" [items]="paintings" (itemTap)="paintingItemTap($event)" separatorColor="#2c2b30" (loaded)="onListViewLoaded($event)" >
                <ng-template let-item="item" let-odd="odd">
                    <PaintingItem [painting]="item" [odd]="odd"></PaintingItem>
                </ng-template>
            </ListView>
        </GridLayout>`
})
export class HomeListComponent implements OnInit, TabSelected {
    searchPhrase = "";

    paintings = [];

    filterOptions: any = {};

    inProgress = false;

    @ViewChild('listSearchInput')
    private searchInput: ElementRef;

    constructor(private routerExtensions: RouterExtensions, private paintingService: PaintingService, private page: Page) {
        this.page.on(Page.navigatedToEvent, () => {
            if (isAndroid) {
                let searchInput = this.searchInput.nativeElement;
                searchInput.dismissSoftInput();
                app.android.startActivity.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
            const searchBar = <SearchBar>this.searchInput.nativeElement;
            if (searchBar.ios) {
                searchBar.ios.endEditing(true);
            } else if (searchBar.android) {
                searchBar.android.clearFocus();
            }
        });
    }

    onClear(args) {
        let searchBar = <SearchBar>args.object;
        this.searchPhrase = searchBar.text = "";
        this.search();
    }

    onSubmit(event) {
        let searchBar = <SearchBar>event.object;
        this.searchPhrase = searchBar.text;
        this.search();
    }

    tabSelected(home: HomeComponent) {
        if (!this.paintingService.paintingsLoaded && this.paintingService.loadingPromise) {
            this.inProgress = true;
            this.paintingService.loadingPromise.then(() => this.search())
                .catch(err => {
                    this.inProgress = true;
                    console.log(JSON.stringify(err));
                    Toast.makeText("Unable to loading data", "long").show();
                })
        } else {
            this.search();
        }
    }

    tabDeselected(home: HomeComponent) {
        const searchBar = <SearchBar>this.searchInput.nativeElement;
        if (searchBar.ios) {
            searchBar.ios.endEditing(true);
        } else if (searchBar.android) {
            searchBar.android.clearFocus();
        }
        this.paintings = [];
    }

    search() {
        if (!this.filterOptions) this.filterOptions = {};
        this.inProgress = true;
        this.paintingService.getPaintingsByFilterOptions(this.filterOptions, this.searchPhrase)
            .then(r => {
                this.paintings = r;
                this.inProgress = false;
            })
            .catch(err => {
                this.inProgress = false;
                console.log(JSON.stringify(err));
                Toast.makeText("Unable to filter data", "long").show();
            })
    }

    ngOnInit(): void {
        this.page.on(Page.navigatedToEvent, (e) => {
            if (prefs.hasKey(FILTER_OPTIONS_KEY)) {
                this.filterOptions = JSON.parse(prefs.getString(FILTER_OPTIONS_KEY));
                this.search();
            }
        });
        this.page.actionBar.getViewById("homeListFilterButton").on("tap", () => {
            this.routerExtensions.navigate(["/filter"])
        });
    }

    paintingItemTap(args) {
        this.routerExtensions.navigate(["/painting", this.paintings[args.index].id],);
    }

    onListViewLoaded(event){
        const lv = event.object;
        if (lv.android) {
            const id = utils.ad.resources.getId('home_list_list_view');
            lv.android.setId(id);
        }
    }

}