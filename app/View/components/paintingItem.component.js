"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PaintingItemComponent = (function () {
    function PaintingItemComponent() {
        this.painting = {};
    }
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], PaintingItemComponent.prototype, "painting", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], PaintingItemComponent.prototype, "odd", void 0);
    PaintingItemComponent = __decorate([
        core_1.Component({
            selector: 'PaintingItem',
            moduleId: module.id,
            template: "\n        <!--<GridLayout columns=\"*\" rows=\"*\"-->\n                    <!--class=\"painting-item {{ painting.resolved && painting.resolved != 'false' ? 'painting-resolved' : ''}}\"-->\n                    <!--padding=\"0\">-->\n            <!--<Image stretch=\"aspectFit\" [src]=\"painting.tn\" row=\"0\"></Image>-->\n            <!--<StackLayout class=\"painting-item__text\" row=\"0\" width=\"100%\">-->\n                <!--<FlexboxLayout class=\"painting-item__text-bg\" justifyContent=\"space-between\">-->\n                    <!--<StackLayout orientation=\"horizontal\">-->\n                        <!--<Label class=\"painting-item__name\" [text]=\"painting.name\"></Label>-->\n                        <!--<Label class=\"painting-item__info\" [text]=\"',  ' + painting.authorName\"></Label>-->\n                    <!--</StackLayout>-->\n                    <!--<Image [src]=\"painting.resolved && painting.resolved != 'false' ? '~/assets/img/marker-green.png': '~/assets/img/marker-red.png'\"-->\n                           <!--height=\"20\" stretch=\"aspectFit\" marginLeft=\"5\"></Image>-->\n                <!--</FlexboxLayout>-->\n            <!--</StackLayout>-->\n        <!--</GridLayout>-->\n<FlexboxLayout class=\"selected-painting painting-item\" justifyContent=\"center\" backgroundColor=\"white\">\n    <StackLayout class=\"selected-painting__center\">\n        <Image [src]=\"painting.tn ? painting.tn : ''\" ></Image>\n        <StackLayout class=\"selected-painting__label\" width=\"80%\">\n            <Label class=\"selected-painting__name\" [text]=\"painting.name\" color=\"black\"></Label>\n            <Label class=\"selected-painting__info\" color=\"black\"\n                   text=\"{{ painting.authorName?painting.authorName:('UnknownAuthor'|L)}}{{painting.paintingCreated?', '+painting.paintingCreated:''}}\"></Label>\n            <Label [text]=\"(painting.resolved&&painting.resolved!='false'?'ResolvedLabel':'NotResolvedLabel')|L\"\n                   [class]=\"painting.resolved&&painting.resolved!='false'?'resolved-label':'not-resolved-label'\"></Label>\n        </StackLayout>\n    </StackLayout>\n</FlexboxLayout>"
        }),
        __metadata("design:paramtypes", [])
    ], PaintingItemComponent);
    return PaintingItemComponent;
}());
exports.PaintingItemComponent = PaintingItemComponent;
