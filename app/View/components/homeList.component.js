"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var nativescript_angular_1 = require("nativescript-angular");
var painting_service_1 = require("../../Service/painting.service");
var page_1 = require("tns-core-modules/ui/page");
var prefs = require("application-settings");
var platform_1 = require("tns-core-modules/platform");
var app = require("tns-core-modules/application");
var Toast = require("nativescript-toast");
var utils = require("utils/utils");
exports.FILTER_OPTIONS_KEY = "artopos.filter_options";
var FilterOptions = (function () {
    function FilterOptions() {
    }
    return FilterOptions;
}());
exports.FilterOptions = FilterOptions;
var HomeListComponent = (function () {
    function HomeListComponent(routerExtensions, paintingService, page) {
        var _this = this;
        this.routerExtensions = routerExtensions;
        this.paintingService = paintingService;
        this.page = page;
        this.searchPhrase = "";
        this.paintings = [];
        this.filterOptions = {};
        this.inProgress = false;
        this.page.on(page_1.Page.navigatedToEvent, function () {
            if (platform_1.isAndroid) {
                var searchInput = _this.searchInput.nativeElement;
                searchInput.dismissSoftInput();
                app.android.startActivity.getWindow().setSoftInputMode(android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
            var searchBar = _this.searchInput.nativeElement;
            if (searchBar.ios) {
                searchBar.ios.endEditing(true);
            }
            else if (searchBar.android) {
                searchBar.android.clearFocus();
            }
        });
    }
    HomeListComponent.prototype.onClear = function (args) {
        var searchBar = args.object;
        this.searchPhrase = searchBar.text = "";
        this.search();
    };
    HomeListComponent.prototype.onSubmit = function (event) {
        var searchBar = event.object;
        this.searchPhrase = searchBar.text;
        this.search();
    };
    HomeListComponent.prototype.tabSelected = function (home) {
        var _this = this;
        if (!this.paintingService.paintingsLoaded && this.paintingService.loadingPromise) {
            this.inProgress = true;
            this.paintingService.loadingPromise.then(function () { return _this.search(); })
                .catch(function (err) {
                _this.inProgress = true;
                console.log(JSON.stringify(err));
                Toast.makeText("Unable to loading data", "long").show();
            });
        }
        else {
            this.search();
        }
    };
    HomeListComponent.prototype.tabDeselected = function (home) {
        var searchBar = this.searchInput.nativeElement;
        if (searchBar.ios) {
            searchBar.ios.endEditing(true);
        }
        else if (searchBar.android) {
            searchBar.android.clearFocus();
        }
        this.paintings = [];
    };
    HomeListComponent.prototype.search = function () {
        var _this = this;
        if (!this.filterOptions)
            this.filterOptions = {};
        this.inProgress = true;
        this.paintingService.getPaintingsByFilterOptions(this.filterOptions, this.searchPhrase)
            .then(function (r) {
            _this.paintings = r;
            _this.inProgress = false;
        })
            .catch(function (err) {
            _this.inProgress = false;
            console.log(JSON.stringify(err));
            Toast.makeText("Unable to filter data", "long").show();
        });
    };
    HomeListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.on(page_1.Page.navigatedToEvent, function (e) {
            if (prefs.hasKey(exports.FILTER_OPTIONS_KEY)) {
                _this.filterOptions = JSON.parse(prefs.getString(exports.FILTER_OPTIONS_KEY));
                _this.search();
            }
        });
        this.page.actionBar.getViewById("homeListFilterButton").on("tap", function () {
            _this.routerExtensions.navigate(["/filter"]);
        });
    };
    HomeListComponent.prototype.paintingItemTap = function (args) {
        this.routerExtensions.navigate(["/painting", this.paintings[args.index].id]);
    };
    HomeListComponent.prototype.onListViewLoaded = function (event) {
        var lv = event.object;
        if (lv.android) {
            var id = utils.ad.resources.getId('home_list_list_view');
            lv.android.setId(id);
        }
    };
    __decorate([
        core_1.ViewChild('listSearchInput'),
        __metadata("design:type", core_1.ElementRef)
    ], HomeListComponent.prototype, "searchInput", void 0);
    HomeListComponent = __decorate([
        core_1.Component({
            selector: "HomeList",
            moduleId: module.id,
            template: "\n        <GridLayout backgroundColor=\"white\" rows=\"auto,*\">\n            <SearchBar hint=\"Zadejte n\u00E1zev d\u00EDla nebo jm\u00E9no autora\" #listSearchInput\n                       [text]=\"searchPhrase\"\n                       (submit)=\"onSubmit($event)\"\n                       (clear)=\"onClear($event)\"\n                       textFieldHintColor=\"#cdcdcd\"\n                       color=\"#ffffff\"\n                       row=\"0\"\n                       backgroundColor=\"#2c2b30\"></SearchBar>\n\n            <ActivityIndicator row=\"1\" [busy]=\"inProgress\" class=\"activity-indicator\"></ActivityIndicator>\n\n            <Label row=\"1\" [visibility]=\"!inProgress && !paintings.length ? 'visible':'collapse'\"\n                   [text]=\"'NoPaintings'|L\" textAlignment=\"center\"></Label>\n\n            <ListView row=\"1\" [items]=\"paintings\" (itemTap)=\"paintingItemTap($event)\" separatorColor=\"#2c2b30\" (loaded)=\"onListViewLoaded($event)\" >\n                <ng-template let-item=\"item\" let-odd=\"odd\">\n                    <PaintingItem [painting]=\"item\" [odd]=\"odd\"></PaintingItem>\n                </ng-template>\n            </ListView>\n        </GridLayout>"
        }),
        __metadata("design:paramtypes", [nativescript_angular_1.RouterExtensions, painting_service_1.PaintingService, page_1.Page])
    ], HomeListComponent);
    return HomeListComponent;
}());
exports.HomeListComponent = HomeListComponent;
