import {Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef} from "@angular/core";
import {FILTER_OPTIONS_KEY} from "./components/homeList.component";
import {RouterExtensions} from "nativescript-angular";
import {Page} from "tns-core-modules/ui/page";
import * as prefs from "tns-core-modules/application-settings";
import {Bounds, Position} from "nativescript-google-maps-sdk";
import {debounce} from "../Helpers/debounce";
import {GeocodingService} from "../Service/geocoding.service";
import {localize} from "nativescript-localize";
import {ApiService} from "../Service/api.service";
import {ListPicker} from "tns-core-modules/ui/list-picker";

@Component({
    selector: "Filter",
    moduleId: module.id,
    templateUrl: "./filter.html",
})
export class FilterComponent {

    filterOptions : any = {};

    resolvedOptions = [
        localize('None'),
        localize('Resolved'),
        localize('NotResolved'),
    ];

    institutions = [];

    // search
    private searchItems = [];

    @ViewChild('resolvedPicker')
    private resolvedPicker: ElementRef;

    resIndex = 0;

    @ViewChild('institutionPicker')
    private institutionPicker: ElementRef;

    instIndex = 0;

    @ViewChild('searchInput')
    private searchInput: ElementRef;

    constructor(private routerExtensions: RouterExtensions, private page: Page, private geocoding: GeocodingService, private api: ApiService, private cdr: ChangeDetectorRef) {
        this.page.on(Page.navigatingFromEvent, e => {
            const insts = <ListPicker>this.institutionPicker.nativeElement;
            if (!insts.selectedIndex) {
                this.filterOptions.institutionName = null;
            } else {
                this.filterOptions.institutionName = this.institutions[insts.selectedIndex];
            }
            const rslvd = <ListPicker>this.resolvedPicker.nativeElement;
            switch (rslvd.selectedIndex) {
                case 0:
                    this.filterOptions.resolved = null;
                    break;
                case 1:
                    this.filterOptions.resolved = true;
                    break;
                case 2:
                    this.filterOptions.resolved = false;
                    break;
            }
            prefs.setString(FILTER_OPTIONS_KEY, JSON.stringify(this.filterOptions));
        });
        this.api.get("/institutions").then(r => {
            this.institutions = [localize('None')];
            r.forEach((ins, i) => {
                this.institutions.push(ins.name);
                if (ins.name == this.filterOptions.institutionName) {
                    this.instIndex = i+1;
                }
            });
            this.cdr.detectChanges();
        });

        this.page.on(Page.navigatedToEvent, e => {
            let options = prefs.getString(FILTER_OPTIONS_KEY);
            if (options) {
                this.filterOptions = JSON.parse(options);
            } else {
                return;
            }
            if (this.filterOptions.resolved === true) {
                this.resIndex = 1;
            } else if (this.filterOptions.resolved === false) {
                this.resIndex = 2;
            } else {
                this.resIndex = 0;
            }
            if (this.institutions) {
                this.institutions.forEach((ins, i) => {
                    if (ins.name == this.filterOptions.institutionName) {
                        this.instIndex = i+1;
                    }
                })
            }
            this.cdr.detectChanges();
        });
    }

    removeFilters() {
        this.instIndex = 0;
        this.resIndex = 0;
        this.clearAddress();
        this.filterOptions = {};
    }

    // Search
    search = debounce((args = null) => {
        let txt = this.searchInput.nativeElement.text;
        if (txt.length < 3) return;
        this.geocoding.getPlaces(txt).then((results: any) => {
            this.searchItems = results;
        });
    }, 700);

    resultClick(placeItem) {
        if (!placeItem) return;
        this.geocoding.getPlace(placeItem).then(place => {
            this.filterOptions.address = place.data.result.formatted_address;
            const bounds = place.data.result.geometry.viewport;
            this.filterOptions.bounds = {
                southwest: {
                    latitude: bounds.southwest.lat,
                    longitude: bounds.southwest.lng
                },
                northeast: {
                    latitude: bounds.northeast.lat,
                    longitude: bounds.northeast.lng
                }
            };
            this.searchItems = [];
            this.searchInput.nativeElement.text = "";
        });
    }

    onItemTap($event) {
        this.resultClick(this.searchItems[$event.index]);
    }

    clearSearch() {
        this.searchItems = [];
        this.searchInput.nativeElement.text = "";
    }

    clearAddress() {
        this.filterOptions.bounds = null;
        this.filterOptions.address = "";
    }


}