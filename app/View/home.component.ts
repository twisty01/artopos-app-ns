import {AfterViewInit, Component, ElementRef, ViewChild} from "@angular/core";
import {BottomBar, BottomBarItem, SelectedIndexChangedEventData, TITLE_STATE} from "nativescript-bottombar";
import * as platform from "tns-core-modules/platform";
import {HomeProfileComponent} from "./components/homeProfile.component";
import {HomeListComponent} from "./components/homeList.component";
import {HomeMapComponent} from "./components/homeMap.component";
import {Page} from "tns-core-modules/ui/page";
import {TabView} from "tns-core-modules/ui/tab-view";
import {localize} from "nativescript-localize";

const WIDTH = platform.screen.mainScreen.widthPixels;
const DURATION = 100;

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.html"
})
export class HomeComponent implements AfterViewInit {
    @ViewChild('homeTabView') private homeTabView: ElementRef;
    @ViewChild(HomeMapComponent) private map: HomeMapComponent;
    @ViewChild(HomeListComponent) private list: HomeListComponent;
    @ViewChild(HomeProfileComponent) private profile: HomeProfileComponent;

    private tabContents;

    // bottom bar style properties
    private _bar: BottomBar;
    private titleState: TITLE_STATE;
    private inactiveColor: string;
    private accentColor: string;
    // tabs
    private tv: TabView;
    private tabsRemoved = false;
    private readonly DEFAULT_INDEX = 1;
    public oldIndex = this.DEFAULT_INDEX;
    public selectedIndex: number = this.DEFAULT_INDEX;

    bottomItems: Array<BottomBarItem> = [
        new BottomBarItem(0, localize("List"), "ic_format_list_bulleted_white_36dp", "#222125"),
        new BottomBarItem(1, localize("Map"), "ic_map_white_36dp", "#2c2b30"),
        new BottomBarItem(2, localize("Profile"), "ic_account_white_36dp", "#3a3a40"),
    ];

    constructor(private page: Page) {}

    ngAfterViewInit() {
        this.tabContents = [
            this.list,
            this.map,
            this.profile
        ];
        this.page.on(Page.navigatedToEvent,()=>{
            this.itemCallbacks(this.selectedIndex, this.oldIndex);
        })
    }

    tabsLoaded(event) {
        this.tv = <TabView>this.homeTabView.nativeElement;
        if (!this.tv) return;
        this.tv.selectedIndex = this.selectedIndex;
        if ( this.tabsRemoved ) return;
        this.tabsRemoved = true;
        if (this.tv.android) {
            this.tv.android.removeViewAt(0);
        } else {
            this.tv.ios.tabBar.hidden = true;
        }
    }

    indexChanged(event) {
        if (!this.tv) return;
        this.selectedIndex = this.tv.selectedIndex;
        if (this._bar) this._bar.selectItem(this.selectedIndex);
        console.log("HOME INDEX CHANGED: " + this.tv.selectedIndex);
    }

    bottomBarLoaded(event) {
        this._bar = <BottomBar>event.object;
        this.titleState = TITLE_STATE.SHOW_WHEN_ACTIVE;
        this.inactiveColor = "white";
        this.accentColor = "#d84e4d";
        this._bar.selectItem(this.selectedIndex);
    }

    bottomBarSelected(event) {
        if (!this.tv) return;
        if (event.newIndex != this.tv.selectedIndex) {
            this.itemCallbacks(event.newIndex, event.oldIndex);
        }
        this.tv.selectedIndex = event.newIndex;
    }

    private itemCallbacks(newIndex, oldIndex) {
        if (newIndex != oldIndex && this.tabContents) {
            this.oldIndex = oldIndex;
            if (this.tabContents[oldIndex] && this.tabContents[oldIndex].tabDeselected)
                this.tabContents[oldIndex].tabDeselected(this);
            if (this.tabContents[newIndex] && this.tabContents[newIndex].tabSelected)
                this.tabContents[newIndex].tabSelected(this);
        }
    }

    changeIndex(i = null) {
        if (i === null) i = this.oldIndex;
        if (!this.tv) return;
        console.log("HOME CHANGE INDEX : " + i);
        this.tv.selectedIndex = i;
    }
}


